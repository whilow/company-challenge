const signIn = async (login, password, game) => {
  fetch("/api/users/login", {
    method: "post",
    headers: { "Content-Type": "application/json" },
    body: JSON.stringify({
      login: login,
      password: password,
      game: game
    })
  }).then(async res => {
    const { success, error, token, role, game, login } = await res.json();
    if (!success) {
      alert(error);
    } else {
      const currentUser = {
        role: role,
        game: game,
        login: login,
        bearer: `Bearer ${token}`
      };
      if (currentUser.role === "admin") {
        localStorage.setItem("currentUser", JSON.stringify(currentUser));
      } else {
        sessionStorage.setItem("currentUser", JSON.stringify(currentUser));
      }
      onAuthStateChangedCallback(currentUser);
    }
  });
};
const refresh = async currentUser => {
  const res = await fetch("/api/users/refresh", {
    headers: { Authorization: currentUser.bearer }
  });
  if (res.status === 401) {
    signOut();
  } else {
    const { token } = await res.json();
    currentUser.bearer = `Bearer ${token}`;
    if (currentUser.role === "admin") {
      localStorage.setItem("currentUser", JSON.stringify(currentUser));
    } else {
      sessionStorage.setItem("currentUser", JSON.stringify(currentUser));
    }
    onAuthStateChangedCallback(currentUser);
  }
};
const signOut = () => {
  localStorage.removeItem("currentUser");
  onAuthStateChangedCallback(null);
};
let onAuthStateChangedCallback = null;
const onAuthStateChanged = callback => {
  onAuthStateChangedCallback = callback;
};

export default { signIn, refresh, signOut, onAuthStateChanged };
