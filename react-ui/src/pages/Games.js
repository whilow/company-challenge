import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Table, Card, Pagination, Image } from "react-bootstrap";
import dateService from "../services/date";

const Games = ({ currentUser }) => {
  const [games, setGames] = useState(null);
  const [page, setPage] = useState(1);
  const [nextPage, setNextPage] = useState(null);
  const [prevPage, setPrevPage] = useState(null);
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`/api/games?page=${page}`, {
        headers: { Authorization: currentUser.bearer }
      });
      res.json().then(res => {
        setGames(res.data.docs);
        setNextPage(res.data.nextPage);
        setPrevPage(res.data.prevPage);
        setTotalPages(res.data.totalPages);
      });
    }

    fetchData();
  }, [page]);

  return (
    <Card>
      <Card.Header
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <h4>Games</h4> <Link to="/dashboard/games/add">Add game</Link>
      </Card.Header>
      <Table responsive>
        <thead>
          <tr>
            <th>Cover image</th>
            <th>Name</th>
            <th>Title</th>
            <th>Game identifier</th>
            <th>End date</th>
          </tr>
        </thead>
        <tbody>
          {games
            ? games.map((game, key) => (
                <tr key={`game_${key}`}>
                  <td>
                    <div style={{ width: 40, height: 40, marginRight: 16 }}>
                      <Image
                        style={{ height: 40, width: "auto" }}
                        src={game.coverImage}
                        rounded
                        fluid
                      />
                    </div>
                  </td>
                  <td>
                    <Link to={`/dashboard/games/${game._id}`}>{game.name}</Link>
                  </td>
                  <td>{game.title}</td>
                  <td>{game.gameId}</td>
                  <td>{dateService.niceDate(game.endDate)}</td>
                </tr>
              ))
            : null}
        </tbody>
      </Table>
      <Pagination>
        <Pagination.First disabled={page === 1} onClick={() => setPage(1)} />
        <Pagination.Prev
          disabled={!prevPage}
          onClick={() => setPage(prevPage)}
        />
        <Pagination.Next
          disabled={!nextPage}
          onClick={() => setPage(nextPage)}
        />
        <Pagination.Last
          disabled={page === totalPages}
          onClick={() => setPage(totalPages)}
        />
      </Pagination>
    </Card>
  );
};

export default Games;
