import React, { useContext } from "react";
import { withRouter, Redirect } from "react-router-dom";
import { AuthContext } from "../Auth.js";
import LoginForm from "../components/LoginForm";

const Login = () => {
  const { currentUser } = useContext(AuthContext);

  if (currentUser) {
    return <Redirect to="/" />;
  }

  return (
    <div
      style={{
        width: "100%",
        height: "100%",
        display: "flex",
        justifyContent: "center",
        alignItems: "flex-start"
      }}
    >
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          alignItems: "center",
          boxShadow: "5px 5px 20px 0 rgba(0,0,0,0.15)",
          padding: 20,
          marginTop: "15%",
          borderRadius: 5
        }}
      >
        <h1 style={{ fontWeight: "300", marginBottom: 20 }}>Log in</h1>
        <LoginForm />
      </div>
    </div>
  );
};

export default withRouter(Login);
