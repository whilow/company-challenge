import React, { useState, useEffect } from "react";
import { Card, Col, Form, Image } from "react-bootstrap";

const EditQuestion = ({ history, match, currentUser }) => {
  const [question, setQuestion] = useState(null);
  const [questionGames] = useState(["Snake"]);

  useEffect(() => {
    async function fetchData() {
      const res = await fetch(`/api/questions/${match.params.id}`, {
        headers: {
          Authorization: currentUser.bearer
        }
      });
      res.json().then(res => {
        setQuestion({ ...res.data, questionGame: "Snake" });
      });
    }

    fetchData();
  }, []);

  const save = () => {
    fetch("/api/questions", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: currentUser.bearer
      },
      body: JSON.stringify({
        ...question,
        ...{
          answers:
            question.type === "Test" || question.type === "Open"
              ? question.answers
              : [],
          taskPoints: question.type === "Task" ? question.taskPoints : [],
          questionGame: question.type === "Game" ? question.questionGame : null
        }
      })
    }).then(async res => {
      const { success, error } = await res.json();
      if (!success) {
        alert(error);
      } else {
        history.push("/dashboard/questions");
      }
    });
  };

  const remove = () => {
    if (window.confirm("Are you sure you want to delete this record?")) {
      fetch(`/api/questions/${match.params.id}`, {
        method: "delete",
        headers: { Authorization: currentUser.bearer }
      }).then(async res => {
        const { success, error } = await res.json();
        if (!success) {
          alert(error);
        } else {
          history.push("/dashboard/questions");
        }
      });
    }
  };

  return question ? (
    <>
      <Card>
        <Card.Header>
          <h4>Edit Question</h4>
        </Card.Header>
        <Card.Body>
          <Form>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  value={question.title}
                  onChange={ev => {
                    question.title = ev.target.value;
                    setQuestion({ ...question });
                  }}
                  placeholder="Enter title"
                />
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>Type</Form.Label>
                <Form.Control
                  as="select"
                  value={question.type}
                  onChange={ev => {
                    const val = ev.target.value;
                    question.type = val;
                    setQuestion({ ...question });
                  }}
                >
                  <option>Test</option>
                  <option>Open</option>
                  <option>Task</option>
                  <option>Game</option>
                </Form.Control>
              </Form.Group>
            </Form.Row>

            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={question.name}
                onChange={ev => {
                  question.name = ev.target.value;
                  setQuestion({ ...question });
                }}
                placeholder="Enter name"
              />
            </Form.Group>

            {question.type !== "Game" ? (
              <Form.Group>
                <Form.Label>Image</Form.Label>
                <Form.Group style={{ display: "flex", alignItems: "center" }}>
                  {question.image ? (
                    <div style={{ width: 40, height: 40, marginRight: 16 }}>
                      <Image
                        style={{ height: 40, width: "auto" }}
                        src={question.image}
                        rounded
                        fluid
                      />
                    </div>
                  ) : null}
                  <Form.Control
                    style={{ margin: 0 }}
                    value={question.image}
                    onChange={ev => {
                      question.image = ev.target.value;
                      setQuestion({ ...question });
                    }}
                    placeholder="Enter link to the image"
                  />
                </Form.Group>
              </Form.Group>
            ) : (
              <Form.Group>
                <Form.Label>Choose game for the question</Form.Label>
                <Form.Control
                  as="select"
                  value={question.questionGame}
                  onChange={ev => {
                    question.questionGame = ev.target.value;
                    setQuestion({ ...question });
                  }}
                >
                  {questionGames.map((qg, i) => (
                    <option key={`qg-${i}`}>{qg}</option>
                  ))}
                </Form.Control>
              </Form.Group>
            )}

            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Enter description"
                rows={4}
                value={question.description}
                onChange={ev => {
                  question.description = ev.target.value;
                  setQuestion({ ...question });
                }}
              />
            </Form.Group>

            {question.type !== "Game" ? (
              question.type !== "Task" ? (
                <>
                  <Form.Group style={{ marginBottom: 0 }}>
                    <Form.Label>Answers</Form.Label>
                    {question.answers.map((a, index) => (
                      <Form.Group
                        key={index}
                        style={{ display: "flex", marginBottom: 0 }}
                      >
                        <div>
                          <Form.Control
                            placeholder="Enter answer"
                            value={a.value}
                            onChange={ev => {
                              a.value = ev.target.value;
                              setQuestion({ ...question });
                            }}
                          />
                        </div>
                        <div style={{ marginLeft: 16 }}>
                          <Form.Control
                            placeholder="Enter points"
                            value={a.points}
                            onChange={ev => {
                              a.points = Number(ev.target.value);
                              setQuestion({ ...question });
                            }}
                          />
                        </div>
                        <div style={{ marginLeft: 16 }}>
                          <button
                            onClick={ev => {
                              ev.preventDefault();
                              question.answers = [
                                ...question.answers.filter(
                                  (x, i) => i !== index
                                )
                              ];
                              setQuestion({ ...question });
                            }}
                          >
                            Remove
                          </button>
                        </div>
                      </Form.Group>
                    ))}
                  </Form.Group>
                  <button
                    onClick={ev => {
                      ev.preventDefault();
                      question.answers = [
                        ...question.answers,
                        { value: "", points: 0 }
                      ];
                      setQuestion({ ...question });
                    }}
                  >
                    Add
                  </button>
                </>
              ) : (
                <>
                  <Form.Group style={{ marginBottom: 0 }}>
                    <Form.Label>Points to get</Form.Label>
                    {question.taskPoints.map((tp, index) => (
                      <Form.Group
                        key={index}
                        style={{ display: "flex", marginBottom: 0 }}
                      >
                        <div>
                          <Form.Control
                            placeholder="Enter points"
                            value={tp}
                            onChange={ev => {
                              question.taskPoints[index] = Number(
                                ev.target.value
                              );
                              setQuestion({ ...question });
                            }}
                          />
                        </div>
                        <div style={{ marginLeft: 16 }}>
                          <button
                            onClick={ev => {
                              ev.preventDefault();
                              question.taskPoints = [
                                ...question.taskPoints.filter(
                                  (x, i) => i !== index
                                )
                              ];
                              setQuestion({ ...question });
                            }}
                          >
                            Remove
                          </button>
                        </div>
                      </Form.Group>
                    ))}
                  </Form.Group>
                  <button
                    onClick={ev => {
                      ev.preventDefault();
                      question.taskPoints = [...question.taskPoints, 1];
                      setQuestion({ ...question });
                    }}
                  >
                    Add
                  </button>
                </>
              )
            ) : null}
          </Form>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <button style={{ marginRight: 16 }} onClick={() => remove()}>
              Delete
            </button>
            <button className={"primary"} onClick={() => save()}>
              Save
            </button>
          </div>
        </Card.Body>
      </Card>
    </>
  ) : null;
};

export default EditQuestion;
