import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Table, Card, Pagination, Image } from "react-bootstrap";

const Questions = ({ currentUser }) => {
  const [questions, setQuestions] = useState(null);
  const [page, setPage] = useState(1);
  const [nextPage, setNextPage] = useState(null);
  const [prevPage, setPrevPage] = useState(null);
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    const fetchData = async () => {
      const res = await fetch(`/api/questions?page=${page}`, {
        headers: { Authorization: currentUser.bearer }
      });
      res.json().then(res => {
        setQuestions(res.data.docs);
        setNextPage(res.data.nextPage);
        setPrevPage(res.data.prevPage);
        setTotalPages(res.data.totalPages);
      });
    };

    fetchData();
  }, [page]);

  return (
    <Card>
      <Card.Header
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <h4>Questions</h4> <Link to="/dashboard/questions/add">Add question</Link>
      </Card.Header>
      <Table responsive>
        <thead>
          <tr>
            <th>Image</th>
            <th>Title</th>
            <th>Name</th>
            <th>Description</th>
            <th>Type</th>
            <th>Answers</th>
          </tr>
        </thead>
        <tbody>
          {questions
            ? questions.map((question, key) => (
                <tr key={`question_${key}`}>
                  <td>
                    <div style={{ width: 40, height: 40, marginRight: 16 }}>
                      <Image
                        style={{ height: 40, width: "auto" }}
                        src={question.image}
                        rounded
                        fluid
                      />
                    </div>
                  </td>
                  <td>
                    <Link to={`/dashboard/questions/${question._id}`}>
                      {question.title}
                    </Link>
                  </td>
                  <td>{question.name}</td>
                  <td>{question.description}</td>
                  <td>{question.type}</td>
                  <td>
                    {question.answers.map((a, key) => (
                      <span key={key}>
                        <span
                          style={
                            a.points
                              ? {
                                  color: "#1bbc9b",
                                  textDecoration: "underline"
                                }
                              : null
                          }
                        >
                          {a.value}
                        </span>
                        {key !== question.answers.length - 1 ? ", " : null}
                      </span>
                    ))}
                  </td>
                </tr>
              ))
            : null}
        </tbody>
      </Table>
      <Pagination>
        <Pagination.First disabled={page === 1} onClick={() => setPage(1)} />
        <Pagination.Prev
          disabled={!prevPage}
          onClick={() => setPage(prevPage)}
        />
        <Pagination.Next
          disabled={!nextPage}
          onClick={() => setPage(nextPage)}
        />
        <Pagination.Last
          disabled={page === totalPages}
          onClick={() => setPage(totalPages)}
        />
      </Pagination>
    </Card>
  );
};

export default Questions;
