import React, { useEffect, useState, useContext } from "react";
import { AuthContext } from "../Auth.js";
import { Link } from "react-router-dom";
import { Container } from "react-bootstrap";
import ItemsTable from "../components/ItemsTable";
import UserPoints from "../components/UserPoints";

const LiveJury = ({ match }) => {
  const { currentUser } = useContext(AuthContext);
  const [schedule, setSchedule] = useState(null);

  useEffect(() => {
    const fetchSchedule = async () => {
      const res = await fetch(`/api/live/schedule/${match.params.schedule}`, {
        headers: {
          "Content-Type": "application/json",
          Authorization: currentUser.bearer
        }
      });
      res.json().then(res => {
        setSchedule(res.data);
      });
    };

    fetchSchedule();
  }, []);

  return (
    <>
      <Container>
        <div style={{ paddingBottom: 20, paddingTop: 20 }}>
          <Link to={`/${match.params.gameId}`}>{"< Schedule"}</Link>
        </div>
        {schedule ? (
          <>
            <div style={{ fontSize: 30, marginTop: 10 }}>
              {schedule.question.title}
            </div>
            <div>{schedule.question.description}</div>
            <div style={{ marginTop: 8 }}>
              {schedule.question.answers.map((a, key) => (
                <span key={key}>
                  <span
                    style={
                      a.points
                        ? {
                            color: "#1bbc9b",
                            textDecoration: "underline"
                          }
                        : null
                    }
                  >
                    {a.value}
                  </span>
                  {key !== schedule.question.answers.length - 1 ? ", " : null}
                </span>
              ))}
            </div>
            {/* Users answers */}
            <ItemsTable
              title="Users answers"
              fetchLink={`/api/live/usersAnswers/${match.params.schedule}`}
              limit={10}
              saveItemLink={
                schedule.question.type === "Task" ? "/api/userAnswers" : null
              }
              keys={[
                { name: "user.login" },
                { name: "answer" },
                {
                  name: "points",
                  component: (item, onItemChange) => {
                    return (
                      <UserPoints
                        schedule={schedule}
                        userAnswer={item}
                        onPointsChange={onItemChange}
                      />
                    );
                  }
                }
              ]}
            />
          </>
        ) : null}
      </Container>
    </>
  );
};

export default LiveJury;
