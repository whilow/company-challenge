import React, { useEffect, useState, useContext } from "react";
import * as ReactDOM from "react-dom";
import { Helmet } from "react-helmet";
import { Container, Row, Col, Image, Form, Card, Modal } from "react-bootstrap";
import authService from "../services/auth";
import { AuthContext } from "../Auth.js";
import ItemsTable from "../components/ItemsTable";
import Countdown from "../components/Countdown";
import RegisterForm from "../components/RegisterForm";
import LoginForm from "../components/LoginForm";
import { Context, SnakeGame } from "../react-game-snake/lib";

const Game = ({ match }) => {
  const { currentUser } = useContext(AuthContext);
  const [modalShow, setModalShow] = useState(false);
  const [game, setGame] = useState(null);
  const [currentQuestionSchedule, setCurrentQuestionSchedule] = useState(null);
  const [nextQuestionFrom, setNextQuestionFrom] = useState(null);
  const [questionInitialized, setQuestionInitialized] = useState(false);
  const [answer, setAnswer] = useState("");
  const [points, setPoints] = useState(null);
  const [gameStarted, setGameStarted] = useState(false);
  const [gamePoints, setGamePoints] = useState(0);

  const fetchAnswer = async schedule => {
    if (currentUser) {
      const res = await fetch(`/api/userAnswers/${schedule}`, {
        headers: {
          Authorization: currentUser.bearer
        }
      });
      res.json().then(res => {
        setAnswer(res.data || "");
      });
    }
  };

  const fetchGamePoints = async schedule => {
    if (currentUser) {
      const res = await fetch(`/api/userGamePoints/${schedule}`, {
        headers: {
          Authorization: currentUser.bearer
        }
      });
      res.json().then(res => {
        setGamePoints(res.data.toString() || "");
      });
    }
  };

  const fetchQuestion = async () => {
    const res = await fetch(
      `/api/live/${match.params.gameId}/currentQuestion`,
      { headers: { Authorization: currentUser.bearer } }
    );
    res.json().then(async res => {
      if (res.data) {
        setCurrentQuestionSchedule(res.data);
        setQuestionInitialized(true);
        fetchAnswer(res.data._id);
        fetchGamePoints(res.data._id);
      } else {
        const res = await fetch(
          `/api/live/${match.params.gameId}/nextQuestionFrom`,
          { headers: { Authorization: currentUser.bearer } }
        );
        res.json().then(res => {
          if (res.data) {
            setNextQuestionFrom(res.data);
          }
          setQuestionInitialized(true);
        });
      }
    });
  };

  useEffect(() => {
    const fetchGame = async () => {
      const res = await fetch(`/api/live/${match.params.gameId}`);
      res.json().then(res => {
        if (currentUser && res.data._id !== currentUser.game) {
          authService.signOut();
        }
        setGame(res.data);
      });
    };

    fetchGame();
    if (currentUser && currentUser.role === "player") {
      setQuestionInitialized(false);
      setPoints(null);
      setGamePoints(0);
      setCurrentQuestionSchedule(null);
      setNextQuestionFrom(null);
      setAnswer("");
      fetchQuestion();
    }
  }, [currentUser]);

  const saveAnswer = () => {
    fetch("/api/userAnswers", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: currentUser.bearer
      },
      body: JSON.stringify({
        schedule: currentQuestionSchedule._id,
        answer: answer
      })
    }).then(async res => {
      const { success, error, points } = await res.json();
      if (!success) {
        alert(error);
      } else {
        setPoints(points);
      }
    });
  };

  const savePoints = points => {
    fetch("/api/userAnswers", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: currentUser.bearer
      },
      body: JSON.stringify({
        schedule: currentQuestionSchedule._id,
        points: points > gamePoints ? points : 0
      })
    }).then(async res => {
      const { success, error, points } = await res.json();
      if (!success) {
        alert(error);
      } else {
        if (points > gamePoints) {
          setGamePoints(points);
        }
      }
    });
  };

  const onCountdownEnd = () => {
    if (currentUser && currentUser.role === "player") {
      setQuestionInitialized(false);
      setPoints(null);
      setGamePoints(0);
      setCurrentQuestionSchedule(null);
      setNextQuestionFrom(null);
      setAnswer("");
      fetchQuestion();
    }
  };

  const renderYourAnswer = () => {
    switch (currentQuestionSchedule.question.type) {
      case "Test":
        return (
          <div>
            {currentQuestionSchedule.question.answers.map((a, i) => {
              return (
                <Form.Check
                  inline
                  style={{ marginBottom: 20 }}
                  label={a.value}
                  value={a.value}
                  type={"radio"}
                  key={`a-${i}`}
                  disabled={points !== null}
                  checked={answer === a.value}
                  onChange={ev => {
                    setAnswer(ev.target.value);
                  }}
                />
              );
            })}
          </div>
        );
      case "Open":
      case "Task":
        return (
          <Form.Control
            value={answer}
            disabled={points !== null}
            onChange={ev => {
              setAnswer(ev.target.value);
            }}
            placeholder="Enter your answer"
          />
        );
      default:
        return null;
    }
  };

  const renderLogin = type => {
    return (
      <>
        <div style={{ display: "flex", flexDirection: "row" }}>
          <LoginForm type={type} game={game._id} />
          <div style={{ marginLeft: "16px", marginBottom: "16px" }}>
            <button onClick={() => setModalShow(true)}>Register</button>
          </div>
        </div>
      </>
    );
  };

  const renderSaveAnswer = () => {
    return (
      <>
        <button onClick={() => saveAnswer()}>Save answer</button>
      </>
    );
  };

  const getCountdownEndDate = () => {
    return currentQuestionSchedule
      ? currentQuestionSchedule.to
      : nextQuestionFrom;
  };

  const getCountdownTitle = () => {
    return currentQuestionSchedule ? "Question ends in:" : "Next question in:";
  };

  return game ? (
    <>
      <Helmet>
        <title>{game.title}</title>
      </Helmet>
      <Container style={{ paddingBottom: 20 }}>
        {/* Header */}
        <div
          style={{
            width: "100%",
            display: "flex",
            flexDirection: "row",
            justifyContent: "flex-end",
            marginTop: "16px"
          }}
        >
          {currentUser ? (
            <button
              style={{ marginBottom: "16px" }}
              onClick={() => authService.signOut()}
            >
              Log out
            </button>
          ) : (
            renderLogin("row")
          )}
        </div>
        <div
          style={{
            display: "flex",
            alignItems: "flex-start",
            justifyContent: "space-between",
            flexDirection: "column",
            padding: 40,
            height: 400,
            background: `url("${game.coverImage}") no-repeat center center / cover #ddd`
          }}
        >
          <div
            style={{
              fontSize: 50,
              color: "#fff",
              backgroundColor: game.title
                ? "rgba(0,0,0,0.4)"
                : "rgba(0,0,0,0.0)",
              padding: "0 30px"
            }}
          >
            {game.title}
          </div>
        </div>

        {/* Question */}
        <div
          style={{
            display: "flex",
            flexDirection: "column",
            alignItems: "center"
          }}
        >
          {currentUser && questionInitialized ? (
            <>
              <div style={{ fontSize: 30, marginTop: 10 }}>
                Hi {currentUser.login}!
              </div>
              {currentUser.role === "player" ? (
                <>
                  {points !== null ? (
                    JSON.stringify(points) === "-1" ? (
                      <div style={{ fontSize: 30, marginTop: 10 }}>
                        Answer saved
                      </div>
                    ) : (
                      <div style={{ fontSize: 30, marginTop: 10 }}>
                        {JSON.stringify(points) === "0"
                          ? "Wrong answer - "
                          : "Answer correct - "}
                        {points}{" "}
                        {JSON.stringify(points) === "1" ? "point" : "points"}
                      </div>
                    )
                  ) : null}

                  {game.endDate && new Date(game.endDate) <= Date.now() ? (
                    <div style={{ fontSize: 30, marginTop: 10 }}>
                      {"The game has ended, thank you."}
                    </div>
                  ) : !currentQuestionSchedule && !nextQuestionFrom ? (
                    <div style={{ fontSize: 30, marginTop: 10 }}>
                      {"You've answered all questions, thank you."}
                    </div>
                  ) : (
                    <>
                      <Countdown
                        endDate={getCountdownEndDate()}
                        title={getCountdownTitle()}
                        onCountdownEnd={onCountdownEnd}
                      />
                      {currentQuestionSchedule ? (
                        currentQuestionSchedule.question.type === "Game" ? (
                          <>
                            <div style={{ fontSize: 30, marginBottom: 10 }}>
                              {currentQuestionSchedule.question.title}
                            </div>
                            <div style={{ marginBottom: 20 }}>
                              {currentQuestionSchedule.question.description}
                            </div>
                            {!gameStarted ? (
                              <div style={{ fontSize: 30, marginBottom: 10 }}>
                                {gamePoints} points
                              </div>
                            ) : null}
                            <div id="game"></div>
                            {!gameStarted ? (
                              <button
                                onClick={() => {
                                  setGameStarted(true);
                                  ReactDOM.render(
                                    <SnakeGame
                                      colors={{
                                        field: "#bdc3c7",
                                        food: "#9b59b6",
                                        snake: "#3498db"
                                      }}
                                      countOfHorizontalFields={20}
                                      countOfVerticalFields={20}
                                      fieldSize={20}
                                      loopTime={200}
                                      pauseAllowed={true}
                                      restartAllowed={false}
                                      onLoose={(context: Context) => {
                                        ReactDOM.render(
                                          <></>,
                                          document.getElementById("game")
                                        );
                                        setGameStarted(false);
                                        context.updateGame({ pause: true });
                                        savePoints(context.game.points);
                                      }}
                                      onPause={(context: Context) =>
                                        console.log("paused")
                                      }
                                      onRestart={(context: Context) => {
                                        console.log("restarted");
                                      }}
                                      onResume={(context: Context) => {
                                        console.log("resumed");
                                      }}
                                      onWin={(context: Context) => {
                                        console.log("you won");
                                        ReactDOM.render(
                                          <></>,
                                          document.getElementById("game")
                                        );
                                        setGameStarted(false);
                                        context.updateGame({ pause: true });
                                        savePoints(context.game.points);
                                      }}
                                    />,
                                    document.getElementById("game")
                                  );
                                }}
                              >
                                Start
                              </button>
                            ) : null}
                          </>
                        ) : (
                          <>
                            {!currentQuestionSchedule.question.image ? (
                              <div
                                style={{
                                  maxWidth: 600,
                                  display: "flex",
                                  flexDirection: "column",
                                  alignItems: "center"
                                }}
                              >
                                <div style={{ fontSize: 30, marginBottom: 10 }}>
                                  {currentQuestionSchedule.question.title}
                                </div>
                                <div style={{ marginBottom: 20 }}>
                                  {currentQuestionSchedule.question.description}
                                </div>
                                {renderYourAnswer()}
                                {renderSaveAnswer()}
                              </div>
                            ) : (
                              <Row style={{ width: "100%" }}>
                                <Col>
                                  <Image
                                    src={currentQuestionSchedule.question.image}
                                    width={"100%"}
                                    height={"auto"}
                                  />
                                </Col>
                                <Col>
                                  <div
                                    style={{
                                      display: "flex",
                                      flexDirection: "column",
                                      alignItems: "center"
                                    }}
                                  >
                                    <div
                                      style={{ fontSize: 30, marginBottom: 10 }}
                                    >
                                      {currentQuestionSchedule.question.title}
                                    </div>
                                    <div style={{ marginBottom: 20 }}>
                                      {
                                        currentQuestionSchedule.question
                                          .description
                                      }
                                    </div>
                                    {renderYourAnswer()}
                                    {renderSaveAnswer()}
                                  </div>
                                </Col>
                              </Row>
                            )}
                          </>
                        )
                      ) : null}
                    </>
                  )}
                </>
              ) : null}
            </>
          ) : null}
        </div>

        {/* Page (html) */}
        <div
          style={{ width: "100%" }}
          dangerouslySetInnerHTML={{ __html: game.html }}
        />

        {/* Schedule */}
        {currentUser && currentUser.role !== "player" ? (
          <ItemsTable
            title="Schedule"
            fetchLink={`/api/live/${match.params.gameId}/schedule`}
            limit={10}
            keys={[
              {
                name: "question.title",
                link: `/${match.params.gameId}`,
                linkParam: "_id"
              },
              { name: "question.type", title: "type" },
              { name: "from", type: "date" },
              { name: "to", type: "date" }
            ]}
          />
        ) : null}

        {/* Ranking */}
        <ItemsTable
          key={`ranking-${points}-${gamePoints}`}
          title="Ranking"
          rowNumber={true}
          fetchLink={`/api/live/${match.params.gameId}/ranking`}
          limit={10}
          keys={[{ name: "login" }, { name: "points" }]}
        />

        {/* Materials, Contact */}
        <Card style={{ marginTop: 20 }}>
          <div style={{ display: "flex", flexDirection: "row" }}>
            <div style={{ width: "50%" }}>
              <Card.Header
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <h4>Materials</h4>
              </Card.Header>
              <Card.Body>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column"
                  }}
                >
                  <a href={game.rules} target="_blank">
                    Regulamin
                  </a>
                  {game.materials.map((mat, key) => {
                    return (
                      <a
                        key={`mat-${key}`}
                        href={mat.link}
                        target="_blank"
                        style={{ marginTop: 10 }}
                      >
                        {mat.name}
                      </a>
                    );
                  })}
                </div>
              </Card.Body>
            </div>
            <div style={{ width: "50%" }}>
              <Card.Header
                style={{
                  display: "flex",
                  justifyContent: "space-between",
                  alignItems: "center"
                }}
              >
                <h4>Contact</h4>
              </Card.Header>
              <Card.Body>
                <div
                  style={{
                    display: "flex",
                    flexDirection: "column"
                  }}
                >
                  <div>{game.contact.name}</div>
                  <a
                    style={{ marginTop: 10 }}
                    href={`tel:${game.contact.phone}`}
                  >
                    {game.contact.phone}
                  </a>
                  <a
                    style={{ marginTop: 10 }}
                    href={`mailto:${game.contact.mail}`}
                  >
                    {game.contact.mail}
                  </a>
                </div>
              </Card.Body>
            </div>
          </div>
        </Card>
      </Container>
      <Modal show={modalShow} onHide={() => setModalShow(false)}>
        <Modal.Header closeButton>
          <Modal.Title id="contained-modal-title-vcenter">Register</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          <RegisterForm
            useRecaptcha={true}
            role="player"
            game={game._id}
            onSave={() => setModalShow(false)}
          />
        </Modal.Body>
      </Modal>
    </>
  ) : null;
};

export default Game;
