import React, { useState } from "react";
import { Card, Form, Image } from "react-bootstrap";
import DatePicker from "react-datepicker";

const AddGame = ({ history, currentUser }) => {
  const [title, setTitle] = useState("");
  const [name, setName] = useState("");
  const [coverImage, setCoverImage] = useState("");
  const [html, setHtml] = useState("");
  const [endDate, setEndDate] = useState(null);
  const [gameId, setGameId] = useState("");
  const [contact, setContact] = useState({ name: "", phone: "", mail: "" });
  const [rules, setRules] = useState("");
  const [materials, setMaterials] = useState([]);

  const save = () => {
    fetch("/api/games", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: currentUser.bearer
      },
      body: JSON.stringify({
        title,
        name,
        gameId,
        contact,
        rules,
        materials,
        coverImage,
        html,
        endDate
      })
    }).then(async res => {
      const { success, error } = await res.json();
      if (!success) {
        alert(error);
      } else {
        history.push("/dashboard/games");
      }
    });
  };

  return (
    <>
      <Card>
        <Card.Header>
          <h4>Add Game</h4>
        </Card.Header>
        <Card.Body>
          <Form>
            <Form.Group>
              <Form.Label>Game identifier</Form.Label>
              <Form.Control
                placeholder="Enter game identifier"
                value={gameId}
                onChange={ev => setGameId(ev.target.value)}
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={name}
                onChange={ev => setName(ev.target.value)}
                placeholder="Enter game name"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Title</Form.Label>
              <Form.Control
                value={title}
                onChange={ev => setTitle(ev.target.value)}
                placeholder="Enter game title"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Page (html)</Form.Label>
              <Form.Control
                as="textarea"
                rows={5}
                value={html}
                onChange={ev => setHtml(ev.target.value)}
                placeholder="You can write some html code here"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>End date</Form.Label>
              <div>
                <DatePicker
                  placeholderText="Game end date"
                  selected={endDate ? new Date(Date.parse(endDate)) : null}
                  onChange={date => setEndDate(date)}
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={15}
                  timeCaption="time"
                  dateFormat="MMMM d, yyyy h:mm aa"
                />
              </div>
            </Form.Group>

            <Form.Group>
              <Form.Label>Cover image</Form.Label>
              <Form.Group style={{ display: "flex", alignItems: "center" }}>
                {coverImage ? (
                  <div style={{ width: 40, height: 40, marginRight: 16 }}>
                    <Image
                      style={{ height: 40, width: "auto" }}
                      src={coverImage}
                      rounded
                      fluid
                    />
                  </div>
                ) : null}
                <Form.Control
                  style={{ margin: 0 }}
                  value={coverImage}
                  onChange={ev => setCoverImage(ev.target.value)}
                  placeholder="Enter link to the cover image"
                />
              </Form.Group>
            </Form.Group>

            <Form.Group style={{ marginBottom: 0 }}>
              <Form.Label>Contact</Form.Label>
              <Form.Control
                placeholder="Name"
                value={contact.name}
                onChange={ev =>
                  setContact({ ...contact, name: ev.target.value })
                }
              />
              <Form.Group style={{ display: "flex", marginBottom: 0 }}>
                <Form.Control
                  placeholder="Phone"
                  value={contact.phone}
                  onChange={ev =>
                    setContact({ ...contact, phone: ev.target.value })
                  }
                />
                <Form.Control
                  style={{ marginLeft: 16 }}
                  placeholder="E-mail"
                  value={contact.mail}
                  onChange={ev =>
                    setContact({ ...contact, mail: ev.target.value })
                  }
                />
              </Form.Group>
            </Form.Group>

            <Form.Group>
              <Form.Label>Rules</Form.Label>
              <Form.Control
                placeholder="Enter rules link"
                value={rules}
                onChange={ev => setRules(ev.target.value)}
              />
            </Form.Group>

            <Form.Group style={{ marginBottom: 0 }}>
              <Form.Label>Materials</Form.Label>
              {materials.map((material, index) => {
                return (
                  <Form.Group
                    key={`material-${index}`}
                    style={{ display: "flex", marginBottom: 0 }}
                  >
                    <Form.Control
                      value={material.name}
                      onChange={ev => {
                        material.name = ev.target.value;
                        setMaterials([...materials]);
                      }}
                      placeholder="Enter materials name"
                    />
                    <Form.Control
                      style={{ marginLeft: 16 }}
                      value={material.link}
                      onChange={ev => {
                        material.link = ev.target.value;
                        setMaterials([...materials]);
                      }}
                      placeholder="Enter link to materials"
                    />
                    <div style={{ marginLeft: 16 }}>
                      <button
                        onClick={ev => {
                          ev.preventDefault();
                          setMaterials([
                            ...materials.filter((x, i) => i !== index)
                          ]);
                        }}
                      >
                        Remove
                      </button>
                    </div>
                  </Form.Group>
                );
              })}
            </Form.Group>
            <button
              onClick={ev => {
                ev.preventDefault();
                setMaterials([...materials, { name: "", link: "" }]);
              }}
            >
              Add
            </button>
          </Form>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <button className={"primary"} onClick={() => save()}>
              Save
            </button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
};

export default AddGame;
