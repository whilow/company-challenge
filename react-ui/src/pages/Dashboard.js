import React, { useContext } from "react";
import {
  BrowserRouter as Router,
  Route,
  Switch,
  Link,
  Redirect
} from "react-router-dom";
import authService from "../services/auth";
import { AuthContext } from "../Auth.js";
import { Container, Row, Col } from "react-bootstrap";
import Questions from "./Questions";
import AddQuestion from "./AddQuestion";
import EditQuestion from "./EditQuestion";
import Games from "./Games";
import AddGame from "./AddGame";
import EditGame from "./EditGame";

const Dashboard = () => {
  const { currentUser } = useContext(AuthContext);
  if (!["super-admin", "admin"].includes(currentUser.role)) {
    return <Redirect to="/" />;
  }
  return (
    <Router>
      <Container fluid style={{ height: "100%", backgroundColor: "#fafcfe" }}>
        <Row style={{ height: "100%" }}>
          <Col
            xs={2}
            style={{
              display: "flex",
              flexDirection: "column",
              boxShadow: "5px 5px 15px 0 rgba(0,0,0,0.05)",
              padding: "20px",
              position: "relative",
              zIndex: 11,
              backgroundColor: "#fff"
            }}
          >
            <Link to="/dashboard/questions">Questions</Link>
            <Link to="/dashboard/games">Games</Link>
          </Col>
          <Col xs={10} style={{ padding: 0 }}>
            <div
              style={{
                display: "flex",
                flexDirection: "row",
                justifyContent: "flex-end",
                boxShadow: "5px 5px 15px 0 rgba(0,0,0,0.05)",
                paddingTop: "10px",
                paddingBottom: "10px",
                paddingLeft: "20px",
                paddingRight: "20px",
                position: "relative",
                zIndex: 10,
                backgroundColor: "#fff"
              }}
            >
              <button onClick={() => authService.signOut()}>Log out</button>
            </div>
            <div className="main">
              <Container style={{ padding: "20px" }}>
                <Redirect exact from="/dashboard" to="/dashboard/questions" />
                <Route
                  exact
                  path="/dashboard/questions"
                  render={props => (
                    <Questions {...props} currentUser={currentUser} />
                  )}
                />
                <Switch>
                  <Route
                    exact
                    path="/dashboard/questions/add"
                    render={props => (
                      <AddQuestion {...props} currentUser={currentUser} />
                    )}
                  />
                  <Route
                    path="/dashboard/questions/:id"
                    render={props => (
                      <EditQuestion {...props} currentUser={currentUser} />
                    )}
                  />
                </Switch>
                <Route
                  exact
                  path="/dashboard/games"
                  render={props => (
                    <Games {...props} currentUser={currentUser} />
                  )}
                />
                <Switch>
                  <Route
                    exact
                    path="/dashboard/games/add"
                    render={props => (
                      <AddGame {...props} currentUser={currentUser} />
                    )}
                  />
                  <Route
                    path="/dashboard/games/:id"
                    render={props => (
                      <EditGame {...props} currentUser={currentUser} />
                    )}
                  />
                </Switch>
              </Container>
            </div>
          </Col>
        </Row>
      </Container>
    </Router>
  );
};

export default Dashboard;
