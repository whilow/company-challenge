import React, { useState, useEffect } from "react";
import { Card, Form, Image } from "react-bootstrap";
import ItemsTable from "../components/ItemsTable";
import RegisterForm from "../components/RegisterForm";
import ScheduleForm from "../components/ScheduleForm";
import DatePicker from "react-datepicker";
import NewPassword from "../components/NewPassword";

const EditGame = ({ history, match, currentUser }) => {
  const [game, setGame] = useState(null);

  useEffect(() => {
    async function fetchGame() {
      const res = await fetch(`/api/games/${match.params.id}`, {
        headers: {
          Authorization: currentUser.bearer
        }
      });
      res.json().then(res => {
        let game = res.data;
        game.oldGameId = game.gameId;
        setGame(game);
      });
    }

    fetchGame();
  }, []);

  const save = () => {
    fetch("/api/games", {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: currentUser.bearer
      },
      body: JSON.stringify(game)
    }).then(async res => {
      const { success, error } = await res.json();
      if (!success) {
        alert(error);
      } else {
        history.push("/dashboard/games");
      }
    });
  };

  const remove = () => {
    if (window.confirm("Are you sure you want to delete this record?")) {
      fetch(`/api/games/${match.params.id}`, {
        method: "delete",
        headers: {
          Authorization: currentUser.bearer
        }
      }).then(async res => {
        const { success, error } = await res.json();
        if (!success) {
          alert(error);
        } else {
          history.push("/dashboard/games");
        }
      });
    }
  };

  return game ? (
    <>
      <Card>
        <Card.Header>
          <h4>Edit Game</h4>
        </Card.Header>
        <Card.Body>
          <Form.Group>
            <Form.Label>Game identifier</Form.Label>
            <Form.Control
              placeholder="Enter game identifier"
              value={game.gameId}
              onChange={ev => {
                game.gameId = ev.target.value;
                setGame({ ...game });
              }}
            />
          </Form.Group>

          <Form.Group>
            <Form.Label>Name</Form.Label>
            <Form.Control
              placeholder="Enter game name"
              value={game.name}
              onChange={ev => {
                game.name = ev.target.value;
                setGame({ ...game });
              }}
            />
          </Form.Group>

          <Form>
            <Form.Group>
              <Form.Label>Title</Form.Label>
              <Form.Control
                value={game.title}
                onChange={ev => {
                  game.title = ev.target.value;
                  setGame({ ...game });
                }}
                placeholder="Enter game title"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>Page (html)</Form.Label>
              <Form.Control
                as="textarea"
                rows={5}
                value={game.html}
                onChange={ev => {
                  game.html = ev.target.value;
                  setGame({ ...game });
                }}
                placeholder="You can write some html code here"
              />
            </Form.Group>

            <Form.Group>
              <Form.Label>End date</Form.Label>
              <div>
                <DatePicker
                  placeholderText="Game end date"
                  selected={
                    game.endDate ? new Date(Date.parse(game.endDate)) : null
                  }
                  onChange={date => {
                    game.endDate = date;
                    setGame({ ...game });
                  }}
                  showTimeSelect
                  timeFormat="HH:mm"
                  timeIntervals={15}
                  timeCaption="time"
                  dateFormat="MMMM d, yyyy h:mm aa"
                />
              </div>
            </Form.Group>

            <Form.Group>
              <Form.Label>Cover image</Form.Label>
              <Form.Group style={{ display: "flex", alignItems: "center" }}>
                {game.coverImage ? (
                  <div style={{ width: 40, height: 40, marginRight: 16 }}>
                    <Image
                      style={{ height: 40, width: "auto" }}
                      src={game.coverImage}
                      rounded
                      fluid
                    />
                  </div>
                ) : null}
                <Form.Control
                  style={{ margin: 0 }}
                  value={game.coverImage}
                  onChange={ev => {
                    game.coverImage = ev.target.value;
                    setGame({ ...game });
                  }}
                  placeholder="Enter link to the cover image"
                />
              </Form.Group>
            </Form.Group>

            <Form.Group style={{ marginBottom: 0 }}>
              <Form.Label>Contact</Form.Label>
              <Form.Control
                placeholder="Name"
                value={game.contact.name}
                onChange={ev =>
                  setGame({
                    ...game,
                    contact: { ...game.contact, name: ev.target.value }
                  })
                }
              />
              <Form.Group style={{ display: "flex", marginBottom: 0 }}>
                <Form.Control
                  placeholder="Phone"
                  value={game.contact.phone}
                  onChange={ev =>
                    setGame({
                      ...game,
                      contact: { ...game.contact, phone: ev.target.value }
                    })
                  }
                />
                <Form.Control
                  style={{ marginLeft: 16 }}
                  placeholder="E-mail"
                  value={game.contact.mail}
                  onChange={ev =>
                    setGame({
                      ...game,
                      contact: { ...game.contact, mail: ev.target.value }
                    })
                  }
                />
              </Form.Group>
            </Form.Group>

            <Form.Group>
              <Form.Label>Rules</Form.Label>
              <Form.Control
                placeholder="Enter rules link"
                value={game.rules}
                onChange={ev => {
                  game.rules = ev.target.value;
                  setGame({ ...game });
                }}
              />
            </Form.Group>

            <Form.Group style={{ marginBottom: 0, marginTop: 15 }}>
              <Form.Label>Materials</Form.Label>
              {game.materials.map((material, index) => (
                <Form.Group
                  key={`material-${index}`}
                  style={{ display: "flex", marginBottom: 0 }}
                >
                  <Form.Control
                    value={material.name}
                    onChange={ev => {
                      material.name = ev.target.value;
                      setGame({ ...game });
                    }}
                    placeholder="Enter materials name"
                  />
                  <Form.Control
                    style={{ marginLeft: 16 }}
                    value={material.link}
                    onChange={ev => {
                      material.link = ev.target.value;
                      setGame({ ...game });
                    }}
                    placeholder="Enter link to materials"
                  />
                  <div style={{ marginLeft: 16 }}>
                    <button
                      onClick={ev => {
                        ev.preventDefault();
                        game.materials = [
                          ...game.materials.filter(x => material !== x)
                        ];
                        setGame({ ...game });
                      }}
                    >
                      Remove
                    </button>
                  </div>
                </Form.Group>
              ))}
            </Form.Group>
            <button
              onClick={ev => {
                ev.preventDefault();
                game.materials = [...game.materials, { name: "", link: "" }];
                setGame({ ...game });
              }}
            >
              Add
            </button>
          </Form>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <button style={{ marginRight: 16 }} onClick={() => remove()}>
              Delete
            </button>
            <button className={"primary"} onClick={() => save()}>
              Save
            </button>
          </div>
        </Card.Body>
      </Card>

      {/* Schedule */}
      <ItemsTable
        title="Schedule"
        fetchLink={`/api/schedule/${match.params.id}`}
        keys={[
          { name: "question.title" },
          { name: "from", type: "date" },
          { name: "to", type: "date" }
        ]}
        removeItemLink="/api/schedule"
        form={props => {
          return <ScheduleForm game={match.params.id} {...props} />;
        }}
      />

      {/* Players */}
      <ItemsTable
        title="Players"
        fetchLink={"/api/users"}
        fetchParams={{ game: match.params.id, role: "player" }}
        keys={[
          { name: "login", editable: true },
          {
            name: "newPassword",
            title: "new password",
            component: (item, onItemChange) => {
              return (
                <NewPassword user={item} onPasswordChange={onItemChange} />
              );
            }
          }
        ]}
        removeItemLink="/api/users"
        saveItemLink="/api/users"
        form={props => {
          return (
            <RegisterForm
              type="row"
              role="player"
              game={match.params.id}
              {...props}
            />
          );
        }}
      />

      {/* Jurors */}
      <ItemsTable
        title="Jurors"
        fetchLink={"/api/users"}
        fetchParams={{ game: match.params.id, role: "juror" }}
        keys={[
          { name: "login", editable: true },
          {
            name: "newPassword",
            title: "new password",
            component: (item, onItemChange) => {
              return (
                <NewPassword user={item} onPasswordChange={onItemChange} />
              );
            }
          }
        ]}
        removeItemLink="/api/users"
        saveItemLink="/api/users"
        form={props => {
          return (
            <RegisterForm
              type="row"
              role="juror"
              game={match.params.id}
              {...props}
            />
          );
        }}
      />
    </>
  ) : null;
};

export default EditGame;
