import React, { useContext } from "react";
import { AuthContext } from "../Auth.js";

const Home = ({ history }) => {
  const { currentUser } = useContext(AuthContext);
  if (currentUser && ["super-admin", "admin"].includes(currentUser.role)) {
    history.push("/dashboard");
  }
  return null;
};

export default Home;
