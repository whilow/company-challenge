import React, { useState, useEffect } from "react";
import { Card, Col, Form, Image } from "react-bootstrap";

const AddQuestion = ({ history, currentUser }) => {
  const [title, setTitle] = useState("");
  const [name, setName] = useState("");
  const [type, setType] = useState("Test");
  const [answers, setAnswers] = useState([]);
  const [image, setImage] = useState("");
  const [description, setDescription] = useState("");
  const [taskPoints, setTaskPoints] = useState([]);
  const [questionGames] = useState(["Snake"]);
  const [questionGame, setQuestionGame] = useState("Snake");

  const save = () => {
    fetch("/api/questions", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: currentUser.bearer
      },
      body: JSON.stringify({
        title,
        type,
        answers: type === "Test" || type === "Open" ? answers : [],
        description,
        image,
        name,
        taskPoints: type === "Task" ? taskPoints : [],
        questionGame: type === "Game" ? questionGame : null
      })
    }).then(async res => {
      const { success, error } = await res.json();
      if (!success) {
        alert(error);
      } else {
        history.push("/dashboard/questions");
      }
    });
  };

  return (
    <>
      <Card>
        <Card.Header>
          <h4>Add Question</h4>
        </Card.Header>
        <Card.Body>
          <Form>
            <Form.Row>
              <Form.Group as={Col}>
                <Form.Label>Title</Form.Label>
                <Form.Control
                  value={title}
                  onChange={ev => setTitle(ev.target.value)}
                  placeholder="Enter title"
                />
              </Form.Group>

              <Form.Group as={Col}>
                <Form.Label>Type</Form.Label>
                <Form.Control
                  as="select"
                  value={type}
                  onChange={ev => {
                    const val = ev.target.value;
                    setType(val);
                  }}
                >
                  <option>Test</option>
                  <option>Open</option>
                  <option>Task</option>
                  <option>Game</option>
                </Form.Control>
              </Form.Group>
            </Form.Row>

            <Form.Group>
              <Form.Label>Name</Form.Label>
              <Form.Control
                value={name}
                onChange={ev => setName(ev.target.value)}
                placeholder="Enter name"
              />
            </Form.Group>

            {type !== "Game" ? (
              <Form.Group>
                <Form.Label>Image</Form.Label>
                <Form.Group style={{ display: "flex", alignItems: "center" }}>
                  {image ? (
                    <div style={{ width: 40, height: 40, marginRight: 16 }}>
                      <Image
                        style={{ height: 40, width: "auto" }}
                        src={image}
                        rounded
                        fluid
                      />
                    </div>
                  ) : null}
                  <Form.Control
                    style={{ margin: 0 }}
                    value={image}
                    onChange={ev => setImage(ev.target.value)}
                    placeholder="Enter link to the image"
                  />
                </Form.Group>
              </Form.Group>
            ) : (
              <Form.Group>
                <Form.Label>Choose game for the question</Form.Label>
                <Form.Control
                  as="select"
                  value={questionGame}
                  onChange={ev => {
                    const val = ev.target.value;
                    setQuestionGame(val);
                  }}
                >
                  {questionGames.map((qg, i) => (
                    <option key={`qg-${i}`}>{qg}</option>
                  ))}
                </Form.Control>
              </Form.Group>
            )}
            <Form.Group>
              <Form.Label>Description</Form.Label>
              <Form.Control
                as="textarea"
                placeholder="Enter description"
                rows={4}
                value={description}
                onChange={ev => setDescription(ev.target.value)}
              />
            </Form.Group>
            {type !== "Game" ? (
              type !== "Task" ? (
                <>
                  <Form.Group style={{ marginBottom: 0 }}>
                    <Form.Label>Answers</Form.Label>
                    {answers.map((a, index) => (
                      <Form.Group
                        key={index}
                        style={{ display: "flex", marginBottom: 0 }}
                      >
                        <div>
                          <Form.Control
                            placeholder="Enter answer"
                            value={a.value}
                            onChange={ev => {
                              a.value = ev.target.value;
                              setAnswers([...answers]);
                            }}
                          />
                        </div>
                        <div style={{ marginLeft: 16 }}>
                          <Form.Control
                            placeholder="Enter points"
                            value={a.points}
                            onChange={ev => {
                              a.points = Number(ev.target.value);
                              setAnswers([...answers]);
                            }}
                          />
                        </div>
                        <div style={{ marginLeft: 16 }}>
                          <button
                            onClick={ev => {
                              ev.preventDefault();
                              setAnswers([
                                ...answers.filter((x, i) => i !== index)
                              ]);
                            }}
                          >
                            Remove
                          </button>
                        </div>
                      </Form.Group>
                    ))}
                  </Form.Group>
                  <button
                    onClick={ev => {
                      ev.preventDefault();
                      setAnswers([...answers, { value: "", points: 0 }]);
                    }}
                  >
                    Add
                  </button>
                </>
              ) : (
                <>
                  <Form.Group style={{ marginBottom: 0 }}>
                    <Form.Label>Points to get</Form.Label>
                    {taskPoints.map((tp, index) => (
                      <Form.Group
                        key={index}
                        style={{ display: "flex", marginBottom: 0 }}
                      >
                        <div>
                          <Form.Control
                            placeholder="Enter points"
                            value={tp}
                            onChange={ev => {
                              taskPoints[index] = Number(ev.target.value);
                              setTaskPoints([...taskPoints]);
                            }}
                          />
                        </div>
                        <div style={{ marginLeft: 16 }}>
                          <button
                            onClick={ev => {
                              ev.preventDefault();
                              setTaskPoints([
                                ...taskPoints.filter((x, i) => i !== index)
                              ]);
                            }}
                          >
                            Remove
                          </button>
                        </div>
                      </Form.Group>
                    ))}
                  </Form.Group>
                  <button
                    onClick={ev => {
                      ev.preventDefault();
                      setTaskPoints([...taskPoints, 1]);
                    }}
                  >
                    Add
                  </button>
                </>
              )
            ) : null}
          </Form>
          <div style={{ display: "flex", justifyContent: "flex-end" }}>
            <button className={"primary"} onClick={() => save()}>
              Save
            </button>
          </div>
        </Card.Body>
      </Card>
    </>
  );
};

export default AddQuestion;
