export function generateRandomDecimalBetween(x1, x2) {
    if (x1 < 0 || x2 < 0) {
        throw new Error("Do not use negative numbers.");
    }
    if (x1 === x2) {
        throw new Error("Both numbers are equal.");
    }
    var min = x1 < x2 ? x1 : x2;
    var max = x1 > x2 ? x1 : x2;
    return Math.floor(Math.random() * (max - min + 1) + min);
}
