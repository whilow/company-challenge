import { Context } from "./game/Context";
import { IFieldsHandlerEvents } from "./game/FieldsHandler";
import { IGameEvents } from "./game/GameHandler";
import { IInputEvents } from "./game/InputHandler";
import { IFieldPosition } from "./model/FieldPosition";
import { IOptions } from "./model/Options";
export interface IGameLogicEvents {
    onLoose: (context: Context) => void;
    onWin: (context: Context) => void;
    onRestart: (context: Context) => void;
    onPause: (context: Context) => void;
    onResume: (context: Context) => void;
    onLoopStart: (context: Context) => void;
    onLoopFinish: (context: Context) => void;
}
export declare class GameLogic implements IInputEvents, IFieldsHandlerEvents, IGameEvents {
    private events;
    private context;
    private loop?;
    private canvasHandler;
    private fieldsHandler;
    private gameHandler;
    private inputHandler;
    private snakeHandler;
    private foodHandler;
    constructor(options: IOptions, canvas: HTMLCanvasElement, events: IGameLogicEvents);
    onLeft(): void;
    onUp(): void;
    onRight(): void;
    onDown(): void;
    onPause(): void;
    onRestart(): void;
    onSnakeIsOutOfBounce(position: IFieldPosition): void;
    onSnakeTouchesHerself(position: IFieldPosition): void;
    onSnakeTouchesFood(position: IFieldPosition): void;
    onStart(): void;
    onLoop(): void;
    start(): void;
    stop(): void;
}
