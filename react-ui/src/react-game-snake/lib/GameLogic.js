import { CanvasHandler } from "./game/CanvasHandler";
import { Context } from "./game/Context";
import { FieldsHandler } from "./game/FieldsHandler";
import { FoodHandler } from "./game/FoodHandler";
import { GameHandler } from "./game/GameHandler";
import { InputHandler } from "./game/InputHandler";
import { SnakeHandler } from "./game/SnakeHandler";
var GameLogic = (function () {
    function GameLogic(options, canvas, events) {
        this.events = events;
        this.context = new Context(options, canvas);
        this.canvasHandler = new CanvasHandler(this.context);
        this.fieldsHandler = new FieldsHandler(this.context, this);
        this.gameHandler = new GameHandler(this.context, this);
        this.inputHandler = new InputHandler(this.context, this);
        this.snakeHandler = new SnakeHandler(this.context);
        this.foodHandler = new FoodHandler(this.context);
    }
    GameLogic.prototype.onLeft = function () {
        this.snakeHandler.setNextDirection("left");
    };
    GameLogic.prototype.onUp = function () {
        this.snakeHandler.setNextDirection("up");
    };
    GameLogic.prototype.onRight = function () {
        this.snakeHandler.setNextDirection("right");
    };
    GameLogic.prototype.onDown = function () {
        this.snakeHandler.setNextDirection("down");
    };
    GameLogic.prototype.onPause = function () {
        if (this.context.options.pauseAllowed) {
            if (this.context.game.pause) {
                this.gameHandler.resume();
                this.events.onResume(this.context);
            }
            else {
                this.gameHandler.pause();
                this.events.onPause(this.context);
            }
        }
    };
    GameLogic.prototype.onRestart = function () {
        if (this.context.options.restartAllowed) {
            this.events.onRestart(this.context);
        }
    };
    GameLogic.prototype.onSnakeIsOutOfBounce = function (position) {
        this.events.onLoose(this.context);
    };
    GameLogic.prototype.onSnakeTouchesHerself = function (position) {
        this.events.onLoose(this.context);
    };
    GameLogic.prototype.onSnakeTouchesFood = function (position) {
        this.snakeHandler.eat();
        this.foodHandler.regenerate();
        this.gameHandler.increasePoints();
    };
    GameLogic.prototype.onStart = function () {
        this.fieldsHandler.rebuild();
        this.foodHandler.regenerate();
    };
    GameLogic.prototype.onLoop = function () {
        this.events.onLoopStart(this.context);
        this.snakeHandler.move();
        this.fieldsHandler.rebuild();
        this.fieldsHandler.check();
        this.canvasHandler.clear();
        this.canvasHandler.draw();
        this.events.onLoopFinish(this.context);
    };
    GameLogic.prototype.start = function () {
        this.inputHandler.startKeyboardInput();
        this.gameHandler.startLoop();
    };
    GameLogic.prototype.stop = function () {
        this.inputHandler.stopKeyboardInput();
        this.gameHandler.stopLoop();
    };
    return GameLogic;
}());
export { GameLogic };
