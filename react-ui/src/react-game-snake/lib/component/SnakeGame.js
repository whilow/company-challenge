import * as React from "react";
import { GameLogic } from "../GameLogic";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var SnakeGame = (function (_super) {
    __extends(SnakeGame, _super);
    function SnakeGame() {
        var _this = _super !== null && _super.apply(this, arguments) || this;
        _this.state = {};
        _this.gameLogic = null;
        return _this;
    }
    SnakeGame.prototype.componentDidUpdate = function (prevProps, prevState) {
        if (this.state.canvas && this.state.canvas !== prevState.canvas) {
            if (this.gameLogic) {
                this.gameLogic.stop();
            }
            this.gameLogic = new GameLogic(this.props, this.state.canvas, this);
            this.gameLogic.start();
        }
        else {
            if (this.gameLogic) {
                this.gameLogic.stop();
            }
            this.gameLogic = null;
        }
    };
    SnakeGame.prototype.render = function () {
        var _this = this;
        var _a = this.props, countOfHorizontalFields = _a.countOfHorizontalFields, countOfVerticalFields = _a.countOfVerticalFields, fieldSize = _a.fieldSize;
        var width = countOfHorizontalFields * fieldSize;
        var height = countOfVerticalFields * fieldSize;
        return (React.createElement("canvas", { ref: function (canvas) { return !_this.state.canvas ? _this.setState({ canvas: canvas }) : null; }, style: {
                height: height,
                width: width,
            }, width: width, height: height }));
    };
    SnakeGame.prototype.onLoose = function (context) {
        console.log('test');
        if (this.props.onLoose) {
            this.props.onLoose(context);
        }
        // this.resetGame();
        this.gameLogic.stop();
    };
    SnakeGame.prototype.onWin = function (context) {
        console.log('test');
        if (this.props.onWin) {
            this.props.onWin(context);
        }
        this.gameLogic.stop();
        // this.resetGame();
    };
    SnakeGame.prototype.onRestart = function (context) {
        if (this.props.onRestart) {
            this.props.onRestart(context);
        }
        this.resetGame();
    };
    SnakeGame.prototype.onPause = function (context) {
        if (this.props.onPause) {
            this.props.onPause(context);
        }
    };
    SnakeGame.prototype.onResume = function (context) {
        if (this.props.onResume) {
            this.props.onResume(context);
        }
    };
    SnakeGame.prototype.onLoopStart = function (context) {
        if (this.props.onLoopStart) {
            this.props.onLoopStart(context);
        }
    };
    SnakeGame.prototype.onLoopFinish = function (context) {
        if (this.props.onLoopFinish) {
            this.props.onLoopFinish(context);
        }
    };
    SnakeGame.prototype.resetGame = function () {
        if (this.gameLogic) {
            this.gameLogic.stop();
            this.gameLogic = null;
        }
        if (this.state.canvas) {
            this.gameLogic = new GameLogic(this.props, this.state.canvas, this);
            this.gameLogic.start();
        }
    };
    return SnakeGame;
}(React.Component));
export { SnakeGame };
