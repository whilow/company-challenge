import * as React from "react";
import { Context } from "../game/Context";
import { IGameLogicEvents } from "../GameLogic";
import { IOptions } from "../model/Options";
interface ISnakeGameProps extends Partial<IGameLogicEvents>, IOptions {
}
interface ISnakeGameState {
    canvas?: HTMLCanvasElement;
}
export declare class SnakeGame extends React.Component<ISnakeGameProps, {}> implements IGameLogicEvents {
    state: ISnakeGameState;
    private gameLogic;
    componentDidUpdate(prevProps: ISnakeGameProps, prevState: ISnakeGameState): void;
    render(): JSX.Element;
    onLoose(context: Context): void;
    onWin(context: Context): void;
    onRestart(context: Context): void;
    onPause(context: Context): void;
    onResume(context: Context): void;
    onLoopStart(context: Context): void;
    onLoopFinish(context: Context): void;
    private resetGame;
}
export {};
