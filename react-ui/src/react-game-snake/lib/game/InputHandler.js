import { Handler } from "./Handler";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var InputHandler = (function (_super) {
    __extends(InputHandler, _super);
    function InputHandler(context, events) {
        var _this = _super.call(this, context) || this;
        _this.events = events;
        return _this;
    }
    InputHandler.prototype.startKeyboardInput = function () {
        document.onkeydown = this.onKeyboardPress.bind(this);
    };
    InputHandler.prototype.stopKeyboardInput = function () {
        document.onkeydown = null;
    };
    InputHandler.prototype.onKeyboardPress = function (event) {
        switch (event.keyCode) {
            case 65:
            case 37:
                this.events.onLeft();
                break;
            case 87:
            case 38:
                this.events.onUp();
                break;
            case 68:
            case 39:
                this.events.onRight();
                break;
            case 83:
            case 40:
                this.events.onDown();
                break;
            case 80:
                this.events.onPause();
                break;
            case 82:
                this.events.onRestart();
                break;
        }
    };
    return InputHandler;
}(Handler));
export { InputHandler };
