import { generateRandomDecimalBetween } from "../utils/MathUtils";
import { Handler } from "./Handler";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

var FoodHandler = (function (_super) {
    __extends(FoodHandler, _super);
    function FoodHandler() {
        return _super !== null && _super.apply(this, arguments) || this;
    }
    FoodHandler.prototype.regenerate = function () {
        var fields = this.context.fields;
        var emptyFields = [];
        for (var _i = 0, _a = Object.keys(fields); _i < _a.length; _i++) {
            var key = _a[_i];
            var field = fields[key];
            if (field.content.length === 0) {
                emptyFields.push(field);
            }
        }
        if (emptyFields.length === 0) {
            throw new Error("There are no empty fields to generate food on.");
        }
        var randomEmptyField = emptyFields[generateRandomDecimalBetween(0, emptyFields.length - 1)];
        this.context.food = __assign({}, randomEmptyField.position);
    };
    return FoodHandler;
}(Handler));
export { FoodHandler };
