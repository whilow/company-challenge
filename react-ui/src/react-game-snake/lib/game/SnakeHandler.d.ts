import { Direction } from "../model/Direction";
import { Context } from "./Context";
import { Handler } from "./Handler";
export declare class SnakeHandler extends Handler {
    private nextDirection;
    constructor(context: Context);
    setNextDirection(direction: Direction): void;
    eat(): void;
    move(): void;
    private prepareDirection;
}
