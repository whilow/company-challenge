import { Handler } from "./Handler";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var FieldsHandler = (function (_super) {
    __extends(FieldsHandler, _super);
    function FieldsHandler(context, events) {
        var _this = _super.call(this, context) || this;
        _this.events = events;
        return _this;
    }
    FieldsHandler.prototype.rebuild = function () {
        this.context.fields = {};
        this.addEmptyFields();
        this.addSnake();
        this.addFood();
    };
    FieldsHandler.prototype.check = function () {
        this.checkIsSnakeOutOfBounce();
        this.checkForCollisions();
    };
    FieldsHandler.prototype.checkIsSnakeOutOfBounce = function () {
        var _a = this.context.options, countOfHorizontalFields = _a.countOfHorizontalFields, countOfVerticalFields = _a.countOfVerticalFields;
        for (var _i = 0, _b = this.context.snake.parts; _i < _b.length; _i++) {
            var part = _b[_i];
            if (part.x >= countOfHorizontalFields ||
                part.y >= countOfVerticalFields ||
                part.x < 0 ||
                part.y < 0) {
                this.events.onSnakeIsOutOfBounce(part);
            }
        }
    };
    FieldsHandler.prototype.checkForCollisions = function () {
        var fields = this.context.fields;
        for (var _i = 0, _a = Object.keys(fields); _i < _a.length; _i++) {
            var key = _a[_i];
            var _b = fields[key], content = _b.content, position = _b.position;
            if (content.length > 1) {
                var snakePresent = content.indexOf("snake") > -1;
                var foodPresent = content.indexOf("food") > -1;
                if (foodPresent && snakePresent) {
                    this.events.onSnakeTouchesFood(position);
                }
                else if (snakePresent) {
                    this.events.onSnakeTouchesHerself(position);
                }
            }
        }
    };
    FieldsHandler.prototype.addEmptyFields = function () {
        var _a = this.context.options, countOfHorizontalFields = _a.countOfHorizontalFields, countOfVerticalFields = _a.countOfVerticalFields;
        for (var x = 0; x < countOfHorizontalFields; x++) {
            for (var y = 0; y < countOfVerticalFields; y++) {
                var position = { x: x, y: y };
                this.context.fields[this.getMapKey(position)] = {
                    content: [],
                    position: position,
                };
            }
        }
    };
    FieldsHandler.prototype.addSnake = function () {
        var parts = this.context.snake.parts;
        var fields = this.context.fields;
        for (var _i = 0, parts_1 = parts; _i < parts_1.length; _i++) {
            var part = parts_1[_i];
            var mapKey = this.getMapKey(part);
            if (fields[mapKey]) {
                fields[mapKey].content.push("snake");
            }
            else {
                fields[mapKey] = {
                    content: ["snake"],
                    position: part,
                };
            }
        }
    };
    FieldsHandler.prototype.addFood = function () {
        var _a = this.context, fields = _a.fields, food = _a.food;
        fields[this.getMapKey(food)].content.push("food");
    };
    FieldsHandler.prototype.getMapKey = function (position) {
        return position.x + "-" + position.y;
    };
    return FieldsHandler;
}(Handler));
export { FieldsHandler };
