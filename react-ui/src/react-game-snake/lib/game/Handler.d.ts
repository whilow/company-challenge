import { Context } from "./Context";
export declare abstract class Handler {
    protected context: Context;
    constructor(context: Context);
    log(obj: any): void;
    logContext(): void;
}
