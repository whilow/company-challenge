import { IFieldPosition } from "../model/FieldPosition";
import { Context } from "./Context";
import { Handler } from "./Handler";
export interface IFieldsHandlerEvents {
    onSnakeTouchesHerself: (position: IFieldPosition) => void;
    onSnakeTouchesFood: (position: IFieldPosition) => void;
    onSnakeIsOutOfBounce: (position: IFieldPosition) => void;
}
export declare class FieldsHandler extends Handler {
    private events;
    constructor(context: Context, events: IFieldsHandlerEvents);
    rebuild(): void;
    check(): void;
    private checkIsSnakeOutOfBounce;
    private checkForCollisions;
    private addEmptyFields;
    private addSnake;
    private addFood;
    private getMapKey;
}
