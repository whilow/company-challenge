import { Context } from "./Context";
import { Handler } from "./Handler";
export interface IInputEvents {
    onLeft: () => void;
    onUp: () => void;
    onRight: () => void;
    onDown: () => void;
    onPause: () => void;
    onRestart: () => void;
}
export declare class InputHandler extends Handler {
    private events;
    constructor(context: Context, events: IInputEvents);
    startKeyboardInput(): void;
    stopKeyboardInput(): void;
    private onKeyboardPress;
}
