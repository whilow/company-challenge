import { Handler } from "./Handler";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};

var SnakeHandler = (function (_super) {
    __extends(SnakeHandler, _super);
    function SnakeHandler(context) {
        var _this = _super.call(this, context) || this;
        _this.nextDirection = context.snake.direction;
        return _this;
    }
    SnakeHandler.prototype.setNextDirection = function (direction) {
        this.nextDirection = direction;
    };
    SnakeHandler.prototype.eat = function () {
        this.context.updateSnake({ ate: true });
    };
    SnakeHandler.prototype.move = function () {
        this.prepareDirection();
        var _a = this.context.snake, ate = _a.ate, direction = _a.direction, parts = _a.parts;
        var newParts = [];
        if (parts.length === 0) {
            throw new Error("The snake got no parts.");
        }
        var newHead = __assign({}, parts[0]);
        switch (direction) {
            case "left":
                newHead = __assign({}, newHead, { x: newHead.x - 1 });
                break;
            case "up":
                newHead = __assign({}, newHead, { y: newHead.y - 1 });
                break;
            case "right":
                newHead = __assign({}, newHead, { x: newHead.x + 1 });
                break;
            case "down":
                newHead = __assign({}, newHead, { y: newHead.y + 1 });
                break;
        }
        newParts.push(newHead);
        if (parts.length > 1) {
            var sliceTo = ate ? parts.length : parts.length - 1;
            newParts.push.apply(newParts, parts.slice(0, sliceTo));
        }
        this.context.updateSnake({
            ate: false,
            parts: newParts,
        });
    };
    SnakeHandler.prototype.prepareDirection = function () {
        var blockedDirection = "left";
        switch (this.context.snake.direction) {
            case "left":
                blockedDirection = "right";
                break;
            case "up":
                blockedDirection = "down";
                break;
            case "right":
                blockedDirection = "left";
                break;
            case "down":
                blockedDirection = "up";
                break;
        }
        if (this.nextDirection !== blockedDirection) {
            this.context.updateSnake({ direction: this.nextDirection });
        }
    };
    return SnakeHandler;
}(Handler));
export { SnakeHandler };
