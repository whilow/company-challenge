import { Handler } from "./Handler";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var CanvasHandler = (function (_super) {
    __extends(CanvasHandler, _super);
    function CanvasHandler(context) {
        var _this = _super.call(this, context) || this;
        var renderer = _this.context.canvas.getContext("2d");
        if (renderer) {
            _this.renderer = renderer;
        }
        else {
            throw new Error("canvas rendering context couldn't be initialized.");
        }
        return _this;
    }
    CanvasHandler.prototype.clear = function () {
        this.renderer.clearRect(0, 0, this.getFieldWidth(), this.getFieldHeight());
    };
    CanvasHandler.prototype.draw = function () {
        var fields = this.context.fields;
        for (var _i = 0, _a = Object.keys(fields); _i < _a.length; _i++) {
            var key = _a[_i];
            this.drawField(fields[key]);
        }
    };
    CanvasHandler.prototype.drawField = function (field) {
        var _a = this.context.options, fieldSize = _a.fieldSize, colors = _a.colors;
        var color = colors.field;
        if (field.content.indexOf("snake") > -1) {
            color = colors.snake;
        }
        else if (field.content.indexOf("food") > -1) {
            color = colors.food;
        }
        this.renderer.fillStyle = color;
        this.renderer.fillRect(field.position.x * fieldSize, field.position.y * fieldSize, fieldSize, fieldSize);
    };
    CanvasHandler.prototype.getFieldWidth = function () {
        var _a = this.context.options, countOfHorizontalFields = _a.countOfHorizontalFields, fieldSize = _a.fieldSize;
        return countOfHorizontalFields * fieldSize;
    };
    CanvasHandler.prototype.getFieldHeight = function () {
        var _a = this.context.options, countOfVerticalFields = _a.countOfVerticalFields, fieldSize = _a.fieldSize;
        return countOfVerticalFields * fieldSize;
    };
    return CanvasHandler;
}(Handler));
export { CanvasHandler };
