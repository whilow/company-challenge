import { Handler } from "./Handler";

var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();

var GameHandler = (function (_super) {
    __extends(GameHandler, _super);
    function GameHandler(context, events) {
        var _this = _super.call(this, context) || this;
        _this.events = events;
        return _this;
    }
    GameHandler.prototype.increasePoints = function () {
        this.context.updateGame({ points: this.context.game.points + 1 });
    };
    GameHandler.prototype.resetPoints = function () {
        this.context.updateGame({ points: 0 });
    };
    GameHandler.prototype.pause = function () {
        this.context.updateGame({ pause: true });
    };
    GameHandler.prototype.resume = function () {
        this.context.updateGame({ pause: false });
    };
    GameHandler.prototype.startLoop = function () {
        this.events.onStart();
        if (!this.context.game.loop) {
            this.context.updateGame({
                loop: setInterval(this.onLoop.bind(this), this.context.options.loopTime),
            });
        }
    };
    GameHandler.prototype.stopLoop = function () {
        if (this.context.game.loop) {
            clearInterval(this.context.game.loop);
            this.context.updateGame({ loop: undefined });
        }
    };
    GameHandler.prototype.onLoop = function () {
        if (!this.context.game.pause) {
            this.events.onLoop();
        }
    };
    return GameHandler;
}(Handler));
export { GameHandler };
