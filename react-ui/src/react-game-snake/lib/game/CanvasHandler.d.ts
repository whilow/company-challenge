import { Context } from "./Context";
import { Handler } from "./Handler";
export declare class CanvasHandler extends Handler {
    private renderer;
    constructor(context: Context);
    clear(): void;
    draw(): void;
    private drawField;
    private getFieldWidth;
    private getFieldHeight;
}
