import { Context } from "./Context";
import { Handler } from "./Handler";
export interface IGameEvents {
    onLoop: () => void;
    onStart: () => void;
}
export declare class GameHandler extends Handler {
    private events;
    constructor(context: Context, events: IGameEvents);
    increasePoints(): void;
    resetPoints(): void;
    pause(): void;
    resume(): void;
    startLoop(): void;
    stopLoop(): void;
    private onLoop;
}
