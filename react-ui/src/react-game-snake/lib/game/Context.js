var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var Context = (function () {
    function Context(options, canvas) {
        this.options = options;
        this.canvas = canvas;
        if (options.countOfHorizontalFields < 3 ||
            options.countOfVerticalFields < 3) {
            throw new Error("The field have to be at least 3x3 fields.");
        }
        this.fields = {};
        this.snake = {
            ate: false,
            direction: "right",
            parts: [
                { x: 0, y: 0 },
                { x: 0, y: 1 },
                { x: 0, y: 2 },
            ],
        };
        this.game = {
            pause: false,
            points: 0,
        };
        this.food = { x: 2, y: 0 };
    }
    Context.prototype.updateSnake = function (snake) {
        this.snake = __assign({}, this.snake, snake);
    };
    Context.prototype.updateGame = function (game) {
        this.game = __assign({}, this.game, game);
    };
    Context.prototype.updateOptions = function (options) {
        this.options = __assign({}, this.options, options);
    };
    return Context;
}());
export { Context };
