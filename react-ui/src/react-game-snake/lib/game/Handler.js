var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var Handler = (function () {
    function Handler(context) {
        this.context = context;
    }
    Handler.prototype.log = function (obj) {
        console.log(obj);
    };
    Handler.prototype.logContext = function () {
        this.log(__assign({}, this.context));
    };
    return Handler;
}());
export { Handler };
