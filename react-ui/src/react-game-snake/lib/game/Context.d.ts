import { IContext } from "../model/Context";
import { IFieldPosition } from "../model/FieldPosition";
import { IFields } from "../model/Fields";
import { IGame } from "../model/Game";
import { IOptions } from "../model/Options";
import { ISnake } from "../model/Snake";
export declare class Context implements IContext {
    options: IOptions;
    canvas: HTMLCanvasElement;
    fields: IFields;
    snake: ISnake;
    game: IGame;
    food: IFieldPosition;
    constructor(options: IOptions, canvas: HTMLCanvasElement);
    updateSnake(snake: Partial<ISnake>): void;
    updateGame(game: Partial<IGame>): void;
    updateOptions(options: Partial<IOptions>): void;
}
