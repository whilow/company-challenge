import React from "react";
import { Form } from "react-bootstrap";

const UserPoints = ({ userAnswer, schedule, onPointsChange }) => {
  const getPoints = () => {
    if (!userAnswer.points) {
      userAnswer.points = "";
    }

    if (schedule.question.answers.length) {
      const answer = schedule.question.answers.find(
        a => a.value === userAnswer.answer
      );
      return answer ? answer.points : 0;
    }

    return schedule.question.taskPoints.length ? (
      <Form.Group>
        <Form.Control
          as="select"
          value={userAnswer.points}
          onChange={ev => {
            userAnswer.points = ev.target.value;
            onPointsChange(userAnswer);
          }}
        >
          <option>0</option>
          {schedule.question.taskPoints.map((tp, key) => (
            <option key={`${schedule.question._id}-${key}`}>{tp}</option>
          ))}
        </Form.Control>
      </Form.Group>
    ) : (
      <Form.Control
        style={{ width: 80 }}
        value={userAnswer.points}
        onChange={ev => {
          userAnswer.points = ev.target.value;
          onPointsChange(userAnswer);
        }}
        placeholder={"Points"}
      />
    );
  };

  return <div>{getPoints()}</div>;
};

export default UserPoints;
