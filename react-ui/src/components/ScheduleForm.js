import React, { useState, useContext } from "react";
import { Form } from "react-bootstrap";
import { AuthContext } from "../Auth.js";
import DatePicker from "react-datepicker";
import Select from "../components/Select";

const ScheduleForm = props => {
  const { currentUser } = useContext(AuthContext);
  const { onSave, game } = props;
  const initSchedule = {
    game: game,
    from: new Date(),
    to: new Date()
  };
  const [schedule, setSchedule] = useState(initSchedule);

  const saveSchedule = () => {
    fetch("/api/schedule", {
      method: "post",
      headers: {
        "Content-Type": "application/json",
        Authorization: currentUser.bearer
      },
      body: JSON.stringify(schedule)
    }).then(async res => {
      const { success, error } = await res.json();
      if (!success) {
        alert(error);
      } else {
        setSchedule(initSchedule);
        if (onSave) {
          onSave();
        }
      }
    });
  };

  return (
    <Form>
      <Form.Group style={{ display: "flex", marginBottom: 0 }}>
        <Select
          itemTitleProp={"title"}
          itemsQuery={"/api/questions"}
          placeholder={"Enter question title"}
          item={schedule.question}
          onSelected={q => {
            schedule.question = q;
            setSchedule({ ...schedule });
          }}
          onDeselected={q => {
            schedule.question = null;
            setSchedule({ ...schedule });
          }}
        />
        <div style={{ marginLeft: 16 }}>
          <DatePicker
            placeholderText="From"
            selected={new Date(Date.parse(schedule.from))}
            onChange={date => {
              schedule.from = date;
              setSchedule({ ...schedule });
            }}
            showTimeSelect
            timeFormat="HH:mm"
            timeIntervals={15}
            timeCaption="time"
            dateFormat="MMMM d, yyyy h:mm aa"
          />
        </div>
        <div style={{ marginLeft: 16 }}>
          <DatePicker
            placeholderText="To"
            selected={schedule.to}
            onChange={date => {
              schedule.to = date;
              setSchedule({ ...schedule });
            }}
            showTimeSelect
            timeFormat="HH:mm"
            timeIntervals={15}
            timeCaption="time"
            dateFormat="MMMM d, yyyy h:mm aa"
          />
        </div>
        <div style={{ marginLeft: 16 }}>
          <button
            onClick={ev => {
              ev.preventDefault();
              saveSchedule();
            }}
          >
            Add
          </button>
        </div>
      </Form.Group>
    </Form>
  );
};

export default ScheduleForm;
