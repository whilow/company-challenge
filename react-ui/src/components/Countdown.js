import React, { useState, useEffect } from "react";

const Countdown = ({ endDate, title, onCountdownEnd }) => {
  const [timeLeft, setTimeLeft] = useState(false);
  let interval = null;

  useEffect(() => {
    setTimeLeft(calculateTimeLeft(endDate));
    // update every second
    interval = setInterval(() => {
      const timeLeft = calculateTimeLeft(endDate);
      timeLeft ? setTimeLeft(timeLeft) : stop();
    }, 1000);

    return () => {
      stop();
    };
  }, [endDate]);

  const calculateTimeLeft = endDate => {
    let diff = (Date.parse(new Date(endDate)) - Date.parse(new Date())) / 1000;

    // clear countdown when date is reached
    if (diff <= 0) {
      onCountdownEnd();
      return false;
    }

    const timeLeft = {
      years: 0,
      days: 0,
      hours: 0,
      min: 0,
      sec: 0,
      millisec: 0
    };

    // calculate time difference between now and expected date
    if (diff >= 365.25 * 86400) {
      // 365.25 * 24 * 60 * 60
      timeLeft.years = Math.floor(diff / (365.25 * 86400));
      diff -= timeLeft.years * 365.25 * 86400;
    }
    if (diff >= 86400) {
      // 24 * 60 * 60
      timeLeft.days = Math.floor(diff / 86400);
      diff -= timeLeft.days * 86400;
    }
    if (diff >= 3600) {
      // 60 * 60
      timeLeft.hours = Math.floor(diff / 3600);
      diff -= timeLeft.hours * 3600;
    }
    if (diff >= 60) {
      timeLeft.min = Math.floor(diff / 60);
      diff -= timeLeft.min * 60;
    }
    timeLeft.sec = diff;

    return timeLeft;
  };

  const stop = () => {
    clearInterval(interval);
  };

  const addLeadingZeros = value => {
    value = String(value);
    while (value.length < 2) {
      value = "0" + value;
    }
    return value;
  };

  return (
    <>
      <div style={{ fontSize: 30, marginTop: 10 }}>{title}</div>
      <div className="Countdown">
        <span className="Countdown-col">
          <span className="Countdown-col-element">
            <strong>{addLeadingZeros(timeLeft.days || 0)}</strong>
            <span>{timeLeft.days === 1 ? "Day" : "Days"}</span>
          </span>
        </span>

        <span className="Countdown-col">
          <span className="Countdown-col-element">
            <strong>{addLeadingZeros(timeLeft.hours || 0)}</strong>
            <span>Hours</span>
          </span>
        </span>

        <span className="Countdown-col">
          <span className="Countdown-col-element">
            <strong>{addLeadingZeros(timeLeft.min || 0)}</strong>
            <span>Min</span>
          </span>
        </span>

        <span className="Countdown-col">
          <span className="Countdown-col-element">
            <strong>{addLeadingZeros(timeLeft.sec || 0)}</strong>
            <span>Sec</span>
          </span>
        </span>
      </div>
    </>
  );
};

export default Countdown;
