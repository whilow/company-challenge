import React, { useState, useEffect, useContext } from "react";
import { Link } from "react-router-dom";
import { AuthContext } from "../Auth.js";
import { Card, Table, Pagination } from "react-bootstrap";
import dateService from "../services/date";
import { Form } from "react-bootstrap";

const ItemsTable = ({
  fetchLink,
  fetchParams,
  rowNumber,
  limit,
  title,
  keys,
  removeItemLink,
  saveItemLink,
  form
}) => {
  const [items, setItems] = useState([]);
  const [itemsPagination, setItemsPagination] = useState({});
  const { currentUser } = useContext(AuthContext);

  async function fetchItems(page) {
    var queryString = fetchParams
      ? Object.keys(fetchParams)
          .map(key => key + "=" + fetchParams[key])
          .join("&")
      : "";
    const res = await fetch(
      `${fetchLink}?${queryString}&page=${page}&limit=${limit || 5}`,
      {
        headers: {
          Authorization: currentUser ? currentUser.bearer : null
        }
      }
    );
    res.json().then(res => {
      setItemsPagination({
        page: res.data.page,
        nextPage: res.data.nextPage,
        prevPage: res.data.prevPage,
        totalPages: res.data.totalPages
      });
      setItems(res.data.docs);
    });
  }

  useEffect(() => {
    fetchItems(1);
  }, []);

  const saveItem = async item => {
    const res = await fetch(`${saveItemLink}`, {
      method: "put",
      headers: {
        "Content-Type": "application/json",
        Authorization: currentUser.bearer
      },
      body: JSON.stringify(item)
    });
    res.json().then(res => {
      if (!res.success) {
        alert(res.error);
      } else {
        fetchItems(1);
      }
    });
  };

  const removeItem = async item => {
    if (window.confirm("Are you sure you want to delete this record?")) {
      const res = await fetch(`${removeItemLink}/${item._id}`, {
        method: "delete",
        headers: {
          Authorization: currentUser.bearer
        }
      });
      res.json().then(res => {
        if (!res.success) {
          alert(res.error);
        } else {
          fetchItems(1);
        }
      });
    }
  };

  const renderItemValue = (item, value, key) => {
    if (key.component) {
      return key.component(item, newItem => {
        setItems([...items]);
      });
    }

    if (key.type === "date") {
      value = dateService.niceDate(value);
    }

    if (key.link) {
      if (key.linkParam) {
        const splittedLinkParam = key.linkParam.split(".");
        let linkParamValue = item;
        splittedLinkParam.forEach(lp => {
          linkParamValue = linkParamValue[lp];
        });
        return <Link to={key.link + "/" + linkParamValue}>{value}</Link>;
      } else {
        return <Link to={key.link}>{value}</Link>;
      }
    } else {
      return value;
    }
  };

  return (
    <Card style={{ marginTop: 20 }}>
      <Card.Header
        style={{
          display: "flex",
          justifyContent: "space-between",
          alignItems: "center"
        }}
      >
        <h4>{title}</h4>
      </Card.Header>
      <Table responsive>
        <thead>
          <tr>
            {rowNumber ? <th></th> : null}
            {keys.map((key, i) => {
              const splittedKey = key.name.split(".");
              return key.title ? (
                <th key={`${key}-${i}`}>{key.title}</th>
              ) : (
                <th key={`${key}-${i}`}>{splittedKey[0]}</th>
              );
            })}
            {removeItemLink ? <th></th> : null}
            {saveItemLink ? <th></th> : null}
          </tr>
        </thead>
        <tbody>
          {items.map((item, i) => {
            return (
              <tr key={`item-${i}`}>
                {rowNumber ? (
                  <td>
                    {(itemsPagination.page - 1) * limit + (Number(i) + 1)}
                  </td>
                ) : null}
                {keys.map((key, j) => {
                  const splittedKey = key.name.split(".");
                  let value = item;
                  splittedKey.forEach(k => {
                    value = value ? value[k] : value;
                  });
                  return (
                    <td key={`item-${i}-val-${j}`}>
                      {key.editable ? (
                        <Form.Control
                          value={value}
                          onChange={ev => {
                            let tms = items;
                            let x = tms[i];
                            splittedKey.forEach((k, index) => {
                              if (index === splittedKey.length - 1) {
                                x[k] = ev.target.value;
                              } else {
                                x = x ? x[k] : x;
                              }
                            });
                            setItems([...tms]);
                          }}
                        ></Form.Control>
                      ) : (
                        renderItemValue(item, value, key)
                      )}
                    </td>
                  );
                })}
                {saveItemLink ? (
                  <td>
                    <button
                      style={{ float: "right" }}
                      onClick={async ev => {
                        ev.preventDefault();
                        saveItem(item);
                      }}
                    >
                      Save
                    </button>
                  </td>
                ) : null}
                {removeItemLink ? (
                  <td>
                    <button
                      style={{ float: "right" }}
                      onClick={async ev => {
                        ev.preventDefault();
                        removeItem(item);
                      }}
                    >
                      Remove
                    </button>
                  </td>
                ) : null}
              </tr>
            );
          })}
        </tbody>
      </Table>
      <Pagination>
        <Pagination.First
          disabled={itemsPagination.page === 1}
          onClick={() => fetchItems(1)}
        />
        <Pagination.Prev
          disabled={!itemsPagination.prevPage}
          onClick={() => fetchItems(itemsPagination.prevPage)}
        />
        <Pagination.Next
          disabled={!itemsPagination.nextPage}
          onClick={() => fetchItems(itemsPagination.nextPage)}
        />
        <Pagination.Last
          disabled={itemsPagination.page === itemsPagination.totalPages}
          onClick={() => fetchItems(itemsPagination.totalPages)}
        />
      </Pagination>

      <Card.Body style={{ border: 0 }}>
        {form
          ? form({
              onSave: () => {
                fetchItems(1);
              }
            })
          : null}
      </Card.Body>
    </Card>
  );
};

export default ItemsTable;
