import React, { useState } from "react";
import { Form } from "react-bootstrap";
import authService from "../services/auth";

const LoginForm = ({ type, game }) => {
  const initUser = { login: "", password: "" };
  const [user, setUser] = useState({ ...initUser });

  return (
    <Form>
      <Form.Group
        style={
          type === "row"
            ? { display: "flex", alignItems: "flex-end", marginBottom: 0 }
            : { display: "flex", flexDirection: "column", flexGrow: 0 }
        }
      >
        <div>
          <Form.Control
            value={user.login}
            onChange={ev => {
              user.login = ev.target.value;
              setUser({ ...user });
            }}
            placeholder={`Login`}
          />
        </div>
        <div style={type === "row" ? { marginLeft: 16 } : {}}>
          <Form.Control
            value={user.password}
            type="password"
            onChange={ev => {
              user.password = ev.target.value;
              setUser({ ...user });
            }}
            placeholder={`Password`}
          />
        </div>
        <div style={type === "row" ? { marginLeft: 16, marginBottom: 16 } : {}}>
          <button
            className="primary"
            style={type === "row" ? {} : { width: "100%" }}
            onClick={async ev => {
              ev.preventDefault();
              await authService.signIn(user.login, user.password, game);
            }}
          >
            Login
          </button>
        </div>
      </Form.Group>
    </Form>
  );
};

export default LoginForm;
