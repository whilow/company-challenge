import React, { useEffect } from "react";
import { Form } from "react-bootstrap";

const NewPassword = ({ user, onPasswordChange }) => {
  return (
    <Form.Control
      value={user.newPassword || ""}
      onChange={ev => {
        let u = user;
        u.newPassword = ev.target.value;
        onPasswordChange(u);
      }}
    />
  );
};

export default NewPassword;
