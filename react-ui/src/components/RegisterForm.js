import React, { useState, useEffect } from "react";
import { Form } from "react-bootstrap";
import ReCAPTCHA from "react-google-recaptcha";

const RegisterForm = ({ role, game, onSave, type, useRecaptcha }) => {
  const initNewUser = { login: "", password: "" };
  const [newUser, setNewUser] = useState({ ...initNewUser });
  const [recaptcha, setRecaptcha] = useState(null);

  function onChange(value) {
    setRecaptcha(value);
  }

  const registerUser = () => {
    if (recaptcha || !useRecaptcha) {
      const { login, password } = newUser;
      fetch(`/api/users/register/${role}`, {
        method: "post",
        headers: { "Content-Type": "application/json" },
        body: JSON.stringify({
          login: login,
          password: password,
          game: game
        })
      }).then(async res => {
        const { success, error } = await res.json();
        if (!success) {
          alert(error);
        } else {
          setNewUser({ ...initNewUser });
          if (onSave) {
            onSave();
          }
        }
      });
    } else {
      alert("Please use captcha.");
    }
  };

  return (
    <Form>
      <Form.Group
        style={
          type === "row"
            ? { display: "flex", alignItems: "flex-end", marginBottom: 0 }
            : {}
        }
      >
        <div>
          <Form.Control
            value={newUser.login}
            onChange={ev => {
              newUser.login = ev.target.value;
              setNewUser({ ...newUser });
            }}
            placeholder={`Enter new ${role} login`}
          />
        </div>
        <div style={type === "row" ? { marginLeft: 16 } : {}}>
          <Form.Control
            value={newUser.password}
            type="password"
            onChange={ev => {
              newUser.password = ev.target.value;
              setNewUser({ ...newUser });
            }}
            placeholder={`Enter new ${role} password`}
          />
        </div>
        <div style={type === "row" ? { marginLeft: 16, marginBottom: 16 } : {}}>
          <button
            style={type === "row" ? {} : { width: "100%" }}
            onClick={ev => {
              ev.preventDefault();
              registerUser();
            }}
          >
            Register
          </button>
        </div>
      </Form.Group>
      {useRecaptcha ? (
        <div
          style={{
            display: "flex",
            justifyContent: "flex-end",
            marginBottom: "16px"
          }}
        >
          <ReCAPTCHA
            sitekey={process.env.REACT_APP_RECAPTCHA_SITEKEY}
            onChange={onChange}
          />
        </div>
      ) : null}
    </Form>
  );
};

export default RegisterForm;
