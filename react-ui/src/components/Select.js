import React, { useState, useEffect, useContext } from "react";
import { AuthContext } from "../Auth.js";
import { Form, Popover, OverlayTrigger, Spinner } from "react-bootstrap";

const List = (search, itemTitleProp, itemsQuery, onItemSelected) => {
  const [items, setItems] = useState([]);
  const [loading, setLoading] = useState(false);
  const [isBottom, setIsBottom] = useState(false);
  const [page, setPage] = useState(1);
  const [hasNextPage, setHasNextPage] = useState(false);
  const [limit] = useState(6);
  const { currentUser } = useContext(AuthContext);

  const fetchData = async p => {
    const res = await fetch(
      `${itemsQuery}?page=${
        p ? p : page
      }&limit=${limit}&search=${search}&select=${itemTitleProp}`,
      {
        headers: {
          Authorization: currentUser.bearer
        }
      }
    );
    const json = await res.json();
    setHasNextPage(json.data.hasNextPage);
    return json.data.docs;
  };

  useEffect(() => {
    if (!loading) {
      setLoading(true);
      fetchData().then(data => {
        setItems(items.concat(data));
        setLoading(false);
      });
    }
  }, [page]);

  useEffect(() => {
    setLoading(true);
    setPage(1);
    setItems([]);
    fetchData(1).then(data => {
      setItems(data);
      setLoading(false);
    });
  }, [search]);

  const onScroll = event => {
    const elem = event.target;
    const scrollY = elem.scrollTop;
    const visible = elem.clientHeight;
    const pageHeight = elem.scrollHeight;
    const bottomOfPage = visible + scrollY >= pageHeight;
    if (bottomOfPage || pageHeight < visible) {
      if (!loading && !isBottom && hasNextPage) {
        setIsBottom(true);
        setPage(Number(page) + 1);
        console.log("bottom");
      }
    } else {
      setIsBottom(false);
    }
  };

  return (
    <Popover id="popover" style={{ minWidth: 300 }}>
      <Popover.Content
        onScroll={onScroll}
        style={{
          overflow: "scroll",
          height: 164,
          display: "flex",
          flexDirection: "column",
          alignItems: "stretch"
        }}
      >
        {items.map((item, index) => {
          return (
            <div key={`popover-item-${index}`}>
              <button
                className="link-style"
                style={{ width: "100%" }}
                onClick={() => onItemSelected(item)}
              >
                {item[itemTitleProp]}
              </button>
            </div>
          );
        })}
        {loading ? (
          <div style={{ display: "flex", justifyContent: "center" }}>
            <Spinner animation="grow" variant="info" />
          </div>
        ) : null}
      </Popover.Content>
    </Popover>
  );
};

const Select = ({
  onDeselected,
  onSelected,
  placeholder,
  itemTitleProp,
  itemsQuery,
  item
}) => {
  const [search, setSearch] = useState("");

  const onItemSelected = item => {
    setSearch("");
    onSelected(item);
  };

  const onItemDeselected = item => {
    onDeselected(item);
  };

  return (
    <>
      <OverlayTrigger
        trigger="focus"
        placement="bottom"
        overlay={List(search, itemTitleProp, itemsQuery, onItemSelected)}
      >
        {!item ? (
          <Form.Control
            placeholder={placeholder}
            value={search}
            onChange={ev => setSearch(ev.target.value)}
          />
        ) : (
          <></>
        )}
      </OverlayTrigger>
      {item ? (
        <div
          style={{
            padding: "10.5px 20px",
            marginBottom: "16px",
            width: "100%",
            display: "flex",
            justifyContent: "space-between"
          }}
        >
          {item[itemTitleProp]}
          <button
            className="link-style"
            style={{ padding: 0 }}
            onClick={() => onItemDeselected(item)}
          >
            X
          </button>
        </div>
      ) : null}
    </>
  );
};

export default Select;
