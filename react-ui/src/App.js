import React from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import { AuthProvider } from "./Auth";
import PrivateRoute from "./PrivateRoute";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Dashboard from "./pages/Dashboard";
import Live from "./pages/Live";
import LiveJury from "./pages/LiveJury";

const App = () => {
  return (
    <AuthProvider>
      <Router>
        <Switch>
          <Route path="/login" component={Login} />
          <PrivateRoute path="/dashboard" component={Dashboard} />
          <PrivateRoute path="/:gameId/:schedule" component={LiveJury} />
          <Route path="/:gameId" component={Live} />
        </Switch>
        <Route exact path="/" component={Home} />
      </Router>
    </AuthProvider>
  );
};

export default App;
