module.exports = requiredRole => {
  return (req, res, next) => {
    const userRole = req.token ? req.token.role : null;
    let allowedRoles = ["super-admin"];
    switch (requiredRole) {
      case "admin":
        allowedRoles = ["super-admin", "admin"];
        break;
      case "juror":
        allowedRoles = ["super-admin", "admin", "juror"];
        break;
      case "player":
        allowedRoles = ["super-admin", "admin", "player"];
        break;
    }
    if (allowedRoles.includes(userRole)) {
      return next();
    } else {
      return res.status(401).send("Action not allowed");
    }
  };
};
