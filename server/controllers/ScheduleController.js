const express = require("express");
const router = express.Router();
const Schedule = require("../models/Schedule");
const Game = require("../models/Game");
const UserAnswer = require("../models/UserAnswer");

router.get("/:game", (req, res) => {
  const { page, limit, includePoints } = req.query;
  const { game } = req.params;
  Schedule.paginate(
    { game: game },
    {
      limit: limit ? Number(limit) : 30,
      page: page,
      sort: "from",
      populate: [
        {
          path: "question",
          select: includePoints
            ? "title image description answers"
            : "title image description"
        }
      ]
    },
    (err, data) => {
      if (err) {
        return res.json({ success: false, error: err });
      }
      if (includePoints) {
        UserAnswer.find({ user: req.token._id }, (err, userAnswers) => {
          if (err) return res.json({ success: false, error: err });
          let schedule = [];
          data.docs.forEach(d => {
            let s = Object.assign({}, d._doc);
            if (s.question.answers) {
              const userAnswer = userAnswers.find(ua =>
                ua.schedule.equals(s._id)
              );
              if (userAnswer) {
                const answer = s.question.answers.find(
                  a => a.value === userAnswer.answer
                );
                s.points = answer ? answer.points : null;
              }
            }
            schedule.push(s);
          });
          return res.json({
            success: true,
            data: { ...data, docs: schedule }
          });
        });
      } else {
        return res.json({ success: true, data: data });
      }
    }
  );
});

router.post("/", (req, res) => {
  let schedule = new Schedule();

  const { game, question, from, to } = req.body;

  if (!question) {
    return res.json({
      success: false,
      error: "Please choose question"
    });
  }

  if (new Date(from) < new Date() || new Date(to) < new Date()) {
    return res.json({
      success: false,
      error: "Please set schedule dates in future only"
    });
  }

  if (new Date(from) > new Date(to)) {
    return res.json({
      success: false,
      error: "Please set the 'to' date after the 'from' date"
    });
  }

  Game.findOne({ _id: game }, (err, g) => {
    if (
      g.endDate &&
      (new Date(from) > new Date(g.endDate) ||
        new Date(g.endDate) < new Date(to))
    ) {
      return res.json({
        success: false,
        error: "Please set the schedule dates before the end of the game"
      });
    } else {
      Schedule.find({ game: game }, (err, data) => {
        if (
          data.some(
            s =>
              new Date(s.from) < new Date(from) &&
              new Date(s.to) > new Date(from)
          ) ||
          data.some(
            s =>
              new Date(s.from) < new Date(to) && new Date(s.to) > new Date(to)
          )
        ) {
          return res.json({
            success: false,
            error: "There is already a question scheduled in the dates provided"
          });
        } else {
          schedule.game = game;
          schedule.question = question;
          schedule.from = from;
          schedule.to = to;

          schedule.save(err => {
            if (err) return res.json({ success: false, error: err });
            return res.json({ success: true });
          });
        }
      });
    }
  });
});

router.put("/", (req, res) => {
  const { _id, question } = req.body;

  if (!question) {
    return res.json({
      success: false,
      error: "Please choose question"
    });
  }

  Schedule.findByIdAndUpdate(_id, req.body, err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;
  Schedule.findByIdAndRemove({ _id: id }, err => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

module.exports = router;
