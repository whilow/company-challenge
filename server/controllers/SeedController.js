const express = require("express");
const router = express.Router();
const Question = require("../models/Question");
const Game = require("../models/Game");
const User = require("../models/User");
const Schedule = require("../models/Schedule");
const UserAnswer = require("../models/UserAnswer");

router.post("/", (req, res) => {
  // Questions
  Question.deleteMany({}, err => {
    if (!err) {
      const questions = [
        {
          title: "Ile nóg ma stonoga?",
          description: "Wybierz prawidłową odpowiedź.",
          type: "Test",
          answers: [
            { value: "25", points: 0 },
            { value: "125", points: 0 },
            { value: "50", points: 0 },
            { value: "100", points: 1 }
          ]
        },
        {
          title: "2 + 2 = ?",
          description: "Wybierz prawidłową odpowiedź.",
          type: "Test",
          answers: [
            { value: "2", points: 0 },
            { value: "4", points: 1 },
            { value: "8", points: 0 },
            { value: "16", points: 0 }
          ]
        },
        {
          title: "Jak miał na imię Kopernik?",
          description: "Podaj swoją odpowiedź.",
          type: "Open",
          answers: [
            { value: "mikolaj", points: 1 },
            { value: "nicolas", points: 1 }
          ]
        },
        {
          title: "Znajdź jednorożca",
          description: "Prześlij zdjęcie jednorożca.",
          type: "Task",
          answers: []
        }
      ];
      questions.forEach(q => {
        let question = new Question(q);
        question.save();
      });
    }
  });

  // Games
  Game.deleteMany({}, err => {
    if (!err) {
      const games = [
        {
          title: "BSH Challenge",
          gameId: "bsh-challenge",
          materials: [],
          coverImage: "https://upload.wikimedia.org/wikipedia/commons/3/3c/Cityoflondon2019june.jpg",
          contact: "lorem ipsum lorem ipsum lorem ipsum",
          rules: "https://www.rules.com"
        },
        {
          title: "Fujitsu championship",
          gameId: "fj-champ",
          materials: [],
          coverImage: "https://upload.wikimedia.org/wikipedia/commons/3/3c/Cityoflondon2019june.jpg",
          contact: "lorem ipsum lorem ipsum lorem ipsum",
          rules: "https://www.rules.com"
        }
      ];
      games.forEach(g => {
        let game = new Game(g);
        game.save();
      });
    }
  });

  // Users
  User.deleteMany({ $or: [{ role: "player" }, { role: "juror" }] }, () => {});

  // Schedule
  Schedule.deleteMany({}, () => {});

  // UserAnswers
  UserAnswer.deleteMany({}, () => {});

  res.send("Database seeded!");
});

module.exports = router;
