const express = require("express");
const router = express.Router();
const argon2 = require("argon2");
const crypto = require("crypto");
const jwt = require("jsonwebtoken");
const isAuth = require("../middlewares/isAuth");
const roleRequired = require("../middlewares/roleRequired");
const User = require("../models/User");

const signature = process.env.JWT_SIGNATURE;
const expiration = "168h";

router.get("/", isAuth, roleRequired("admin"), (req, res) => {
  const { page, limit, search, game, role } = req.query;
  User.paginate(
    {
      $and: [
        { login: { $regex: search || "", $options: "i" } },
        { role: role },
        { game: game }
      ]
    },
    {
      select: "login game",
      limit: limit ? Number(limit) : 30,
      page: page || 1,
      sort: { login: "asc" }
    },
    (err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: data });
    }
  );
});

router.get("/refresh", isAuth, (req, res) => {
  const token = req.token;

  return token
    ? res.json({
        success: true,
        token: jwt.sign(
          {
            _id: token._id,
            login: token.login,
            role: token.role,
            game: token.game
          },
          signature,
          {
            expiresIn: expiration
          }
        )
      })
    : res.status(401).send("Couldn't refresh");
});

router.post("/register/:role", async (req, res) => {
  const { login, password, game } = req.body;
  const { role } = req.params;
  const rolesWhiteList = ["player", "juror"];

  if (!login || !password) {
    return res.json({
      success: false,
      error: "Please provide user login and password"
    });
  }

  if (!rolesWhiteList.includes(role)) {
    return res.json({
      success: false,
      error: "Role not valid"
    });
  }

  User.findOne({ login: login, game: game }, async (err, u) => {
    if (err) return res.json({ success: false, error: err });
    if (u) {
      return res.json({ success: false, error: "Login taken" });
    } else {
      let user = new User();
      const salt = crypto.randomBytes(32);
      const hashedPassword = await argon2.hash(password, { salt });

      user.login = login;
      user.game = game;
      user.hashedPassword = hashedPassword;
      user.salt = salt.toString("hex");
      user.role = role;

      user.save(err => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
      });
    }
  });
});

router.post("/login", async (req, res) => {
  const { login, password, game } = req.body;

  User.findOne({ login: login, game: game }, async (err, user) => {
    if (err) return res.json({ success: false, error: err });
    if (user) {
      const correctPassword = await argon2.verify(
        user.hashedPassword,
        password
      );
      if (correctPassword) {
        return res.json({
          success: true,
          role: user.role,
          game: user.game,
          login: user.login,
          token: jwt.sign(
            {
              _id: user._id,
              login: user.login,
              role: user.role,
              game: user.game
            },
            signature,
            {
              expiresIn: expiration
            }
          )
        });
      } else {
        return res.json({
          success: false,
          error: "Incorrect login or password"
        });
      }
    } else {
      return res.json({ success: false, error: "Incorrect login or password" });
    }
  });
});

router.put("/", async (req, res) => {
  const { _id, login, newPassword, game } = req.body;

  if (!login) {
    return res.json({
      success: false,
      error: "Please provide login for the user"
    });
  }

  let newData = { login: login };

  if (newPassword) {
    const salt = crypto.randomBytes(32);
    const hashedPassword = await argon2.hash(newPassword, { salt });
    newData.salt = salt.toString("hex");
    newData.hashedPassword = hashedPassword;
  }

  User.findOne(
    { login: login, game: game, _id: { $ne: _id } },
    async (err, u) => {
      if (err) return res.json({ success: false, error: err });
      if (u) {
        return res.json({ success: false, error: "Login taken" });
      } else {
        User.findByIdAndUpdate(_id, newData, err => {
          if (err) return res.json({ success: false, error: err });
          return res.json({ success: true });
        });
      }
    }
  );
});

router.delete("/:id", isAuth, roleRequired("admin"), (req, res) => {
  const { id } = req.params;
  User.findOneAndRemove({ _id: id }, err => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

module.exports = router;
