const express = require("express");
const router = express.Router();
const Question = require("../models/Question");

router.get("/", (req, res) => {
  const { page, limit, search, select } = req.query;
  Question.paginate(
    { title: { $regex: search || "", $options: "i" } },
    {
      select: select ? select.replace(/,/g, " ") : "",
      limit: limit ? Number(limit) : 30,
      page: page,
      sort: { title: "asc" }
    },
    (err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: data });
    }
  );
});

router.get("/:id", (req, res) => {
  const { id } = req.params;
  Question.findOne({ _id: id }, (err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});

router.post("/", (req, res) => {
  const { title } = req.body;

  if (!title) {
    return res.json({
      success: false,
      error: "Please provide question title"
    });
  }

  let question = new Question();
  question = Object.assign(question, req.body);

  question.save(err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

router.put("/", (req, res) => {
  const { _id, title } = req.body;

  if (!title) {
    return res.json({
      success: false,
      error: "Please provide question title"
    });
  }

  Question.findByIdAndUpdate(_id, req.body, err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;
  Question.findByIdAndRemove(id, err => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

module.exports = router;
