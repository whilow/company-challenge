const express = require("express");
const router = express.Router();
const roleRequired = require("../middlewares/roleRequired");
const Game = require("../models/Game");

router.get("/", (req, res) => {
  const { page, limit, search, select } = req.query;
  Game.paginate(
    { title: { $regex: search || "", $options: "i" } },
    {
      select: select ? select.replace(/,/g, " ") : "",
      limit: limit ? Number(limit) : 30,
      page: page || 1,
      sort: { title: "asc" }
    },
    (err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: data });
    }
  );
});

router.get("/:id", (req, res) => {
  const { id } = req.params;
  Game.findOne({ _id: id }).exec((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});

router.post("/", (req, res) => {
  const { name, gameId, materials } = req.body;

  if (!name || !gameId) {
    return res.json({
      success: false,
      error: "Please provide game name and identifier"
    });
  }
  if (materials.some(material => !material.name && !material.link)) {
    return res.json({
      success: false,
      error: "Please provide material name and link"
    });
  }

  Game.findOne({ gameId: gameId }, async (err, g) => {
    if (err) return res.json({ success: false, error: err });
    if (g) {
      return res.json({ success: false, error: "Identifier taken" });
    } else {
      let game = new Game();
      game = Object.assign(game, req.body);
      game.save(err => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true });
      });
    }
  });
});

router.put("/", (req, res) => {
  const { _id, gameId, oldGameId, name } = req.body;

  if (!gameId || !name) {
    return res.json({
      success: false,
      error: "Please provide game name and identifier"
    });
  }
  if (req.body.materials.some(material => !material.name && !material.link)) {
    return res.json({
      success: false,
      error: "Please provide material name and link"
    });
  }

  const updateGame = () => {
    Game.findByIdAndUpdate(_id, req.body, err => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true });
    });
  };

  if (oldGameId !== gameId) {
    Game.findOne({ gameId: gameId }, async (err, g) => {
      if (err) return res.json({ success: false, error: err });
      if (g) {
        return res.json({ success: false, error: "Identifier taken" });
      } else {
        updateGame();
      }
    });
  } else {
    updateGame();
  }
});

router.delete("/:id", (req, res) => {
  const { id } = req.params;
  Game.findByIdAndRemove(id, err => {
    if (err) return res.send(err);
    return res.json({ success: true });
  });
});

module.exports = router;
