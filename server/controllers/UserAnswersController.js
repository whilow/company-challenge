const express = require("express");
const router = express.Router();
const roleRequired = require("../middlewares/roleRequired");
const UserAnswer = require("../models/UserAnswer");
const Schedule = require("../models/Schedule");
const Question = require("../models/Question");
const textService = require("../services/text");

router.get("/:schedule", (req, res) => {
  const { schedule } = req.params;
  const user = req.token._id;
  UserAnswer.findOne(
    { schedule: schedule, user: user },
    "answer",
    (err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: data ? data.answer : null });
    }
  );
});

router.post("/", (req, res) => {
  let { schedule, answer, points } = req.body;
  const user = req.token._id;

  Schedule.findOne({ _id: schedule }, async (err, s) => {
    if (err) return res.json({ success: false, error: err });

    Question.findOne({ _id: s.question }, (err, q) => {
      if (err) return res.json({ success: false, error: err });

      if (!answer && q.type !== "Game") {
        return res.json({
          success: false,
          error: "Please provide the answer"
        });
      }

      if (new Date(s.to) < new Date()) {
        return res.json({ success: false, error: "Time's up!" });
      } else {
        UserAnswer.findOne(
          { schedule: schedule, user: user },
          async (err, ua) => {
            if (err) return res.json({ success: false, error: err });
            if (!s.game.equals(req.token.game)) {
              return res.json({
                success: false,
                error: "You're not registered for this game"
              });
            }
            if (ua && q.type !== "Game") {
              return res.json({
                success: false,
                error: "You've got only one shot!"
              });
            }
            let userAnswer = new UserAnswer();
            userAnswer.user = user;
            userAnswer.schedule = schedule;
            if (answer && q.type === "Open") {
              answer = textService.replacePolishChars(
                answer.toLowerCase().trim()
              );
            }
            userAnswer.answer = answer;

            if (q.type !== "Game") {
              userAnswer.save(err => {
                if (err) return res.json({ success: false, error: err });
                const a = q.answers.find(x => x.value === answer);
                return res.json({
                  success: true,
                  points: q.type === "Task" ? -1 : a ? a.points : 0
                });
              });
            } else {
              UserAnswer.findOne(
                { schedule: schedule, user: user, points: { $gt: 0 } },
                (err, x) => {
                  if (!x) {
                    userAnswer.points = points;
                    userAnswer.save(err => {
                      if (err) return res.json({ success: false, error: err });
                      return res.json({
                        success: true,
                        points: points
                      });
                    });
                  } else if (x && x.points < points) {
                    UserAnswer.findByIdAndUpdate(x._id, { points: 0 }, err => {
                      userAnswer.points = points;
                      userAnswer.save(err => {
                        if (err)
                          return res.json({ success: false, error: err });
                        return res.json({
                          success: true,
                          points: points
                        });
                      });
                    });
                  } else {
                    userAnswer.points = 0;
                    userAnswer.save(err => {
                      if (err) return res.json({ success: false, error: err });
                      return res.json({
                        success: true,
                        points: 0
                      });
                    });
                  }
                }
              );
            }
          }
        );
      }
    });
  });
});

router.put("/", roleRequired("juror"), (req, res) => {
  const { _id, points } = req.body;

  if (isNaN(points)) {
    return res.json({ success: false, error: "Points must be a number" });
  }

  UserAnswer.findByIdAndUpdate(_id, req.body, err => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true });
  });
});

module.exports = router;
