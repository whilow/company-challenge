const express = require("express");
const router = express.Router();
const isAuth = require("../middlewares/isAuth");
const roleRequired = require("../middlewares/roleRequired");
const Game = require("../models/Game");
const Schedule = require("../models/Schedule");
const UserAnswer = require("../models/UserAnswer");
const userLookup = require("../aggregations/userLookup");
const scheduleLookup = require("../aggregations/scheduleLookup");
const userTotalPointsAggr = require("../aggregations/userTotalPointsAggr");

router.get("/:gameId", (req, res) => {
  const { gameId } = req.params;
  Game.findOne({ gameId: gameId }).exec((err, data) => {
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: data });
  });
});

router.get("/:gameId/currentQuestion", isAuth, (req, res) => {
  const now = new Date();
  Schedule.paginate(
    { game: req.token.game, to: { $gt: now }, from: { $lt: now } },
    {
      limit: 1,
      page: 1,
      sort: "from",
      populate: [{ path: "question" }]
    },
    (err, schedules) => {
      if (err) return res.json({ success: false, error: err });
      if (schedules && schedules.docs.length) {
        let schedule = schedules.docs[0];
        UserAnswer.findOne(
          { user: req.token._id, schedule: schedule._id },
          (err, userAnswer) => {
            if (err) return res.json({ success: false, error: err });
            if (!userAnswer) {
              schedules.docs.forEach(s => {
                if (s.question.type === "Test") {
                  s.question.answers = s.question.answers.map(q => {
                    return { value: q.value };
                  });
                } else {
                  s.question.answers = [];
                }
              });
              return res.json({
                success: true,
                data: schedule
              });
            } else {
              return res.json({
                success: true,
                data: schedule.question.type !== "Game" ? null : schedule
              });
            }
          }
        );
      } else {
        return res.json({
          success: true,
          data: null
        });
      }
    }
  );
});

router.get("/:gameId/nextQuestionFrom", isAuth, (req, res) => {
  const now = new Date();
  Schedule.paginate(
    { game: req.token.game, from: { $gt: now } },
    {
      limit: 1,
      page: 1,
      sort: "from"
    },
    (err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({
        success: true,
        data: data.docs.length ? data.docs[0].from : null
      });
    }
  );
});

router.get("/:gameId/ranking", (req, res) => {
  const { page, limit } = req.query;
  const { gameId } = req.params;

  var aggr = UserAnswer.aggregate([
    ...userLookup,
    ...scheduleLookup,
    { $match: { "schedule.game.gameId": gameId } },
    {
      $project: {
        testPoints: userTotalPointsAggr,
        taskPoints: "$points",
        possibleAnswers: "$schedule.question.answers",
        answer: "$answer",
        user: "$user"
      }
    },
    {
      $group: {
        _id: "$user._id",
        testPoints: { $sum: "$testPoints" },
        taskPoints: { $sum: "$taskPoints" },
        possibleAnswers: { $push: "$possibleAnswers" },
        userAnswer: { $push: "$answer" },
        login: { $first: "$user.login" }
      }
    },
    {
      $project: {
        points: { $sum: ["$testPoints", "$taskPoints"] },
        possibleAnswers: 1,
        userAnswer: 1,
        login: 1
      }
    }
  ]);
  UserAnswer.aggregatePaginate(
    aggr,
    {
      limit: limit ? Number(limit) : 30,
      page: page,
      sort: "-points"
    },
    (err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: data });
    }
  );
});

router.get("/:gameId/schedule", isAuth, roleRequired("juror"), (req, res) => {
  const { page, limit } = req.query;
  const { gameId } = req.params;
  Game.findOne({ gameId: gameId }).exec((err, g) => {
    if (err) return res.json({ success: false, error: err });
    Schedule.paginate(
      { game: g._id },
      {
        limit: limit ? Number(limit) : 30,
        page: page,
        sort: { from: "asc" },
        populate: "question"
      },
      (err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data: data });
      }
    );
  });
});

router.get("/schedule/:schedule", isAuth, roleRequired("juror"), (req, res) => {
  const { schedule } = req.params;
  Schedule.findOne({ _id: schedule })
    .populate("question")
    .exec((err, data) => {
      if (err) return res.json({ success: false, error: err });
      return res.json({ success: true, data: data });
    });
});

router.get(
  "/usersAnswers/:schedule",
  isAuth,
  roleRequired("juror"),
  (req, res) => {
    const { schedule } = req.params;
    const { page, limit } = req.query;
    UserAnswer.paginate(
      { schedule: schedule },
      {
        limit: limit ? Number(limit) : 30,
        page: page,
        populate: {
          path: "user",
          select: "login"
        }
      },
      (err, data) => {
        if (err) return res.json({ success: false, error: err });
        return res.json({ success: true, data: data });
      }
    );
  }
);

module.exports = router;
