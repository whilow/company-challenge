const express = require("express");
const router = express.Router();
const roleRequired = require("../middlewares/roleRequired");
const UserAnswer = require("../models/UserAnswer");
const Schedule = require("../models/Schedule");
const Question = require("../models/Question");
const textService = require("../services/text");

router.get("/:schedule", (req, res) => {
  const { schedule } = req.params;
  const user = req.token._id;
  UserAnswer.find({ schedule: schedule, user: user }, (err, data) => {
    let p = 0;
    data.forEach(x => {
      if (x.points > p) {
        p = x.points;
      }
    });
    if (err) return res.json({ success: false, error: err });
    return res.json({ success: true, data: p });
  });
});

module.exports = router;
