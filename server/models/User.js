const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const UserSchema = new Schema({
  login: String,
  hashedPassword: String,
  salt: String,
  role: String,
  game: { type: Schema.Types.ObjectId, ref: "Game" },
});

UserSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("User", UserSchema);
