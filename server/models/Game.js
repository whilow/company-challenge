const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const Schema = mongoose.Schema;

const GameSchema = new Schema({
  title: String,
  name: String,
  gameId: String,
  html: String,
  coverImage: String,
  endDate: Date,
  contact: {
    name: String,
    phone: String,
    mail: String
  },
  rules: String,
  materials: [
    {
      name: String,
      link: String
    }
  ]
});

GameSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Game", GameSchema);
