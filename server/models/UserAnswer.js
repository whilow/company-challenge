const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const aggregatePaginate = require('mongoose-aggregate-paginate-v2');
const Schema = mongoose.Schema;

const UserAnswerSchema = new Schema({
  user: { type: Schema.Types.ObjectId, ref: "User" },
  schedule: { type: Schema.Types.ObjectId, ref: "Schedule" },
  answer: String,
  points: Number
});

UserAnswerSchema.plugin(mongoosePaginate);
UserAnswerSchema.plugin(aggregatePaginate);

module.exports = mongoose.model("UserAnswer", UserAnswerSchema);
