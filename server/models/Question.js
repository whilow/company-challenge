const mongoose = require("mongoose");
const mongoosePaginate = require('mongoose-paginate-v2');
const Schema = mongoose.Schema;

const QuestionSchema = new Schema({
  title: String,
  name: String,
  image: String,
  description: String,
  type: String,
  answers: [{ value: String, points: Number }],
  taskPoints: [Number],
  questionGame: String
});

QuestionSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Question", QuestionSchema);
