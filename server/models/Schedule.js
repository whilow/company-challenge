const mongoose = require("mongoose");
const mongoosePaginate = require("mongoose-paginate-v2");
const Schema = mongoose.Schema;

const ScheduleSchema = new Schema({
  game: { type: Schema.Types.ObjectId, ref: "Game" },
  question: { type: Schema.Types.ObjectId, ref: "Question" },
  from: Date,
  to: Date
});

ScheduleSchema.plugin(mongoosePaginate);

module.exports = mongoose.model("Schedule", ScheduleSchema);
