require("dotenv").config();
const mongoose = require("mongoose");
const express = require("express");
const path = require("path");
const bodyParser = require("body-parser");
const cors = require("cors");
const isAuth = require("./middlewares/isAuth");
const roleRequired = require("./middlewares/roleRequired");
const liveController = require("./controllers/LiveController");
const questionsController = require("./controllers/QuestionsController");
const gamesController = require("./controllers/GamesController");
const scheduleController = require("./controllers/ScheduleController");
const usersController = require("./controllers/UsersController");
const userAnswersController = require("./controllers/UserAnswersController");
const userGamePointsController = require("./controllers/UserGamePointsController");
const seedController = require("./controllers/SeedController");

const app = express();
const PORT = process.env.PORT;

// api config
app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(express.static(path.resolve(__dirname, "../react-ui/build")));
app.listen(PORT, () => console.log(`LISTENING ON PORT ${PORT}`));
// api routes
app.use("/api/live", liveController);
app.use("/api/questions", isAuth, roleRequired("admin"), questionsController);
app.use("/api/games", isAuth, roleRequired("admin"), gamesController);
app.use("/api/schedule", isAuth, roleRequired("admin"), scheduleController);
app.use("/api/users", usersController);
app.use("/api/userAnswers", isAuth, userAnswersController);
app.use("/api/userGamePoints", isAuth, userGamePointsController);
app.use("/api/seed", seedController);
// All remaining requests return the React app, so it can handle routing.
app.get("*", function(request, response) {
  response.sendFile(path.resolve(__dirname, "../react-ui/build", "index.html"));
});

// db config
const dbRoute = process.env.DB_CONN;
mongoose.connect(dbRoute, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  useFindAndModify: false
});
let db = mongoose.connection;
db.once("open", () => console.log("connected to the database"));
db.on("error", console.error.bind(console, "MongoDB connection error:"));
