const polishChars = [
  ["ą", "a"],
  ["ć", "c"],
  ["ę", "e"],
  ["ł", "l"],
  ["ń", "n"],
  ["ó", "o"],
  ["ś", "s"],
  ["ź", "z"],
  ["ż", "z"]
];

const replacePolishChars = value => {
  let replaced = value;
  polishChars.forEach(pc => {
    const re = new RegExp(pc[0], "gi");
    const reUp = new RegExp(pc[0].toUpperCase(), "gi");
    replaced = replaced.replace(re, pc[1]);
    replaced = replaced.replace(reUp, pc[1].toUpperCase());
  });
  console.log(replaced);
  return replaced;
};

module.exports = {
  polishChars: polishChars,
  replacePolishChars: replacePolishChars
};
