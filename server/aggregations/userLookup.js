const User = require("../models/User");

module.exports = [
  {
    $lookup: {
      from: User.collection.name,
      localField: "user",
      foreignField: "_id",
      as: "user"
    }
  },
  { $unwind: "$user" }
];
