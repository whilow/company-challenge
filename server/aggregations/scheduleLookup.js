const Schedule = require("../models/Schedule");
const Question = require("../models/Question");
const Game = require("../models/Game");

module.exports = [
  {
    $lookup: {
      from: Schedule.collection.name,
      localField: "schedule",
      foreignField: "_id",
      as: "schedule"
    }
  },
  { $unwind: "$schedule" },
  {
    $lookup: {
      from: Question.collection.name,
      localField: "schedule.question",
      foreignField: "_id",
      as: "schedule.question"
    }
  },
  { $unwind: "$schedule.question" },
  {
    $lookup: {
      from: Game.collection.name,
      localField: "schedule.game",
      foreignField: "_id",
      as: "schedule.game"
    }
  },
  { $unwind: "$schedule.game" }
];
