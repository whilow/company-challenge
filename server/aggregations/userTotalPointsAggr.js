module.exports = {
  $let: {
    vars: {
      userAnswer: {
        $filter: {
          input: "$schedule.question.answers",
          as: "item",
          cond: {
            $eq: ["$$item.value", "$answer"]
          }
        }
      }
    },
    in: {
      $switch: {
        branches: [
          { case: { $eq: ["$$userAnswer", []] }, then: 0 },
          {
            case: { $not: { $eq: ["$$userAnswer", []] } },
            then: {
              $let: {
                vars: { ua: { $arrayElemAt: ["$$userAnswer", 0] } },
                in: "$$ua.points"
              }
            }
          }
        ]
      }
    }
  }
};
